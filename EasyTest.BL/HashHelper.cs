﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Model.BL
{
    public static class HashHelper
    {
        public static String SHA1HashStringForUTF8String(String s)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(s);

            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);

            return HexStringFromBytes(hashBytes);
        }

        private static String HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();
        }
        public static String RandomString()
        {
            Int32 length;
            Random _rand = new Random();
            length = Math.Max(6, 0);
            byte[] bytes = new byte[length];
            _rand.NextBytes(bytes);
            return Convert.ToBase64String(bytes).Substring(0, length);
        }

        private static String metadata = "EasyTestContext";
        private static String connStr = String.Empty;

        private static readonly String key = String.Empty;



        static HashHelper()
        {
            key = File.ReadAllText(ConfigFilePathFile);   
        }



        private static String ConfigFilePathFile
        {
            get 
            {
                String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "EasyTest", "config.data");
                return path;
            }
        }

        public static String GetConnectionString()
        {
            var dbConfig = ConfigurationManager.GetSection("ConnectionManagerDatabaseServers") as NameValueCollection;
            if (dbConfig != null)
            {
               
                String server = Decrypt(dbConfig["Server"]);
                String db = Decrypt(dbConfig["Database"]);
                String login = Decrypt(dbConfig["Login"]);
                String password = Decrypt(dbConfig["Password"]);

                connStr = String.Format("metadata=res://*/{0}.csdl|res://*/{0}.ssdl|res://*/{0}.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source={1};Initial Catalog={2};Persist Security Info=True;User ID={3};Password={4}\""
                                         , metadata, server, db, login, password);
                return connStr;
            }
            else
            {
                throw new Exception(message: "Помилка підключення до БД. Неможливо прочитати настройки  з app.config. Проблема може бути пов'язана з редагуванням даного файлу. ");

            }
        }

        public static String Decrypt(String TextData)
        {
            var sr = new System.IO.StringReader(key);

            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));

            var prKey = (RSAParameters)xs.Deserialize(sr);

            var csp = new RSACryptoServiceProvider();

            csp.ImportParameters(prKey);

            var bytesCypherText = Convert.FromBase64String(TextData);

            var bytesPlainTextData = csp.Decrypt(bytesCypherText, false);

            return System.Text.Encoding.Unicode.GetString(bytesPlainTextData);
        }


    }
}
