﻿using Repository.Pattern.Ef6;
using System;

namespace EasyTestData
{
    public  class EasyTestContext:DataContext
    {
        public EasyTestContext()
            : base("EasyTestContext")
        {
            Configuration.LazyLoadingEnabled = false;
        }
        public EasyTestContext(String conStr)
            :base(conStr)
        {

        }
    }
}
