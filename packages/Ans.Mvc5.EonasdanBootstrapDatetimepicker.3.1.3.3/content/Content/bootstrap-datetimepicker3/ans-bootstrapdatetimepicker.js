﻿!function ($) {
	$(function () {

		$('.form-datetime').datetimepicker({
			pickDate: true,
			pickTime: true,
			useMinutes: true,
			useCurrent: true,
			showToday: true,
			sideBySide: true,
		});

		$('.form-date').datetimepicker({
			pickDate: true,
			pickTime: false,
			showToday: true,
		});

		$('.form-time').datetimepicker({
			pickDate: false,
			pickTime: true,
			useMinutes: true,
			useCurrent: true,
		});

	});
}(window.jQuery);
