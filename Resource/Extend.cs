﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resource
{
	public class Extend :GlobalRes
	{
		public static String GetString(String key)
		{
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentException("Parameter cannot be null");
            }

            return ResourceManager.GetString(key, Culture) ?? "*****";

        }
	}
}
