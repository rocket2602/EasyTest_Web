//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EasyTest.Entity
{
    using System;
    using System.Collections.Generic;
    using Repository.Pattern.Ef6;
    public partial class News:Entity
    {
        public News()
        {
            this.NewsLangs = new HashSet<NewsLang>();
        }
    
        public int Id { get; set; }
        public string Image { get; set; }
        public int CategoryID { get; set; }
        public int UserId { get; set; }
        public long TimeCreated { get; set; }
    
        public virtual NewsCategory NewsCategory { get; set; }
        public virtual UsersInfo UsersInfo { get; set; }
        public virtual ICollection<NewsLang> NewsLangs { get; set; }
    }
}
