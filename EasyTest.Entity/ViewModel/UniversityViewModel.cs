﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EasyTest.Entity.ViewModel
{
    public class UniversityViewModel : University
    {
        public UniversityViewModel()
        {
            UniversitiesLang = new UniversitiesLang();

            Translated = new List<Int32>();
        }

        public UniversitiesLang UniversitiesLang { get; set; }

        public IEnumerable<Country> Countries { get; set; }


        public List<Int32> Translated { get; set; }

        public DateTime DateTime { get; set; }

      


    }

}
