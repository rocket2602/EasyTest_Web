﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.Entity.ViewModel
{
	public class CathedraViewModel :Cathedra
	{
	
		public Boolean Status { get; set; }
		public Int32[] Translated { get; set; }
	}
}
