﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace EasyTest.Entity.ViewModel
{
    public class NewsViewModel
    {
        public NewsViewModel()
        {
            News = new News();



            NewsLang = new NewsLang();
        }

        public News News { get; set; }

        public NewsLang NewsLang { get; set; }

        public UsersInfo UserInfo { get; set; }

        public NewsCategory NewsCategory { get; set; }

        public List<Int32> Translated { get; set; }

        public DateTime DateTime { get; set; }

        public String GenerateSlug()
        {
            String phrase = String.Format("{0}-{1}", News.Id, NewsLang.Name);

            String str = phrase.ToLower();

            str = Regex.Replace(str, @"\s+", " ");

            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();

            str = Regex.Replace(str, @"\s", "-");

            return str;
        }

    }
}
