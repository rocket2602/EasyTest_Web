﻿using System;
using System.Collections.Generic;

namespace EasyTest.Entity.ViewModel
{
    public class AppViewModel
    {
        /*Entity - main table*/
        public AppViewModel()
        {
            Entity = new App();
            AppChange = new AppChanges();
        }

        public App Entity { get; set; }

        public AppChanges AppChange { get; set; }

        public DateTime DateTime { get; set; }

        public List<Int32> Translated { get; set; }
    }
}
