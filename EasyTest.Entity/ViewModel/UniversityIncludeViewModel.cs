﻿using System;
using ext = Repository.Pattern.Ef6;


namespace EasyTest.Entity.ViewModel
{
    public class UniversityIncludeViewModel<T>  where T: ext.Entity
    {
        public T Entity { get; set; }

        public Boolean Status { get; set; }

        public Int32[] Translated { get; set; }
    }

}
