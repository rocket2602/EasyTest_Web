﻿using EasyTest.Entity.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace EasyTest.Entity
{
    [MetadataType(typeof(NewsMetadata))]
    public partial class News
    {
        internal class NewsMetadata
        {
            [Required(ErrorMessageResourceName = "ImageRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "ImageDisplayName", ResourceType = typeof(Resource.GlobalRes))]
            public String Image { get; set; }

            [Required(ErrorMessageResourceName = "CategoryRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "CategoryDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public Int32 CategoryID { get; set; }


            [DataType(DataType.Time)]
            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
            [Display(Name = "DateTimeDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public long TimeCreated { get; set; }

           

        }
    }



    [MetadataType(typeof(NewsLangMetadata))]
    public partial class NewsLang
    {

        internal class NewsLangMetadata
        {
            [Required(ErrorMessageResourceName = "NameNewsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "NameDispay", ResourceType = typeof(Resource.GlobalRes))]
            public string Name { get; set; }

            [Required(ErrorMessageResourceName = "ShortTextRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "ShortTextDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public String ShortText { get; set; }

            [Required(ErrorMessageResourceName = "TextRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "TextDisplay", ResourceType = typeof(Resource.GlobalRes))]
            [AllowHtml]
            public String Text { get; set; }

            [Required(ErrorMessageResourceName = "KeywordsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "KeywordsDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public String Keywords { get; set; }

        }

    }

    
    [MetadataType(typeof(PageLangMetadata))]
    public partial class PageLang
    {
        internal sealed class PageLangMetadata
        {

            [Required(ErrorMessageResourceName = "NameNewsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            public String Name { get; set; }

            [AllowHtml]
            [Required(ErrorMessageResourceName = "TextNewsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            public String Text { get; set; }

            [Required(ErrorMessageResourceName = "KeywordsNewsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            public String Keywords { get; set; }


            [Required]
            public String Description { get; set; }

        }

    }




    [MetadataType(typeof(DepartamentMetadata))]
    public partial class Departament
    {

        class DepartamentMetadata
        {

            [Required(ErrorMessageResourceName = "NameNewsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            public String Name { get; set; }
        }

    }
    [MetadataType(typeof(AppMetadata))]
    public partial class App
    {
        class AppMetadata
        {
            [Required(ErrorMessageResourceName = "IsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "InstallDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public string Full { get; set; }

            [RegularExpression(@"^(\d+.){3}(\d+)$")]
            [Required(ErrorMessageResourceName = "IsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "VersionDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public string Version { get; set; }  

            [Required(ErrorMessageResourceName = "IsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "CurrentDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public Nullable<bool> IsCurrent { get; set; }

            [Display(Name = "PriceDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public decimal Price { get; set; }

            [Display(Name = "Views", ResourceType = typeof(Resource.GlobalRes))]
            public int Views { get; set; }

            [Display(Name = "DownloadsDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public int Downloads { get; set; }

            //[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:ii}", ApplyFormatInEditMode = true)]
            [Display(Name = "DateTimeDisplay", ResourceType = typeof(Resource.GlobalRes))]
            [Required(ErrorMessageResourceName = "IsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            public System.DateTime TimeCreated { get; set; }

            [Display(Name = "VideoLinksDisplay", ResourceType = typeof(Resource.GlobalRes))]
            public String VideoLinks { get; set; }

            [Display(Name = "ScreenshotsDisplay", ResourceType = typeof(Resource.GlobalRes))]
            [Required(ErrorMessageResourceName = "IsRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            public String Screenshots { get; set; }
          
        }
    }




    [MetadataType(typeof(UniversitiesLangMetadata))]
    public partial class UniversitiesLang
    {
        internal sealed class UniversitiesLangMetadata
        {
           

            [Required(ErrorMessageResourceName = "NameUniversityRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "NameDisplayName", ResourceType = typeof(Resource.GlobalRes))]
            public String Name { get; set; }

            [Required(ErrorMessageResourceName = "NameCityRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "CityDisplayName", ResourceType = typeof(Resource.GlobalRes))]
            public String City { get; set; }

            

        }
    }


    [MetadataType(typeof(UniversityMetadata))]
    public partial class University
    {

        internal sealed class UniversityMetadata
        {
            [Display(Name = "CountryIdDispayName", ResourceType = typeof(Resource.GlobalRes))]
            public int CountryId { get; set; }

            [Required(ErrorMessageResourceName = "LMSIdRequired", ErrorMessageResourceType = typeof(Resource.GlobalRes))]
            [Display(Name = "LMSIdDispayName", ResourceType = typeof(Resource.GlobalRes))]
            public int LMSID { get; set; }

            [Required]
            public String Domain { get; set; }

          
        }

       

    }



}
