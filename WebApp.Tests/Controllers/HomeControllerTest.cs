﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApp;
using WebApp.Controllers;

namespace WebApp.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        public class Version {
            public static bool ValidateVersion(string contents)
            {
                bool val = false;
                if (!string.IsNullOrEmpty(contents))
                {
                    string pattern = "^([0-9]*\\.){3}[0-9]*$";
                    System.Text.RegularExpressions.Regex re =
                                new System.Text.RegularExpressions.Regex(pattern);
                    val = re.IsMatch(contents);
                }
                return val;
            }
        }


     


        [TestMethod()]
        public void ValidateVersionTest()
        {
            string[] contentsPass = {
                    "0.0.0.0",
                    "1.2.3.4",
                    "10.2.3.4",
                    "10.20.3.4",
                    "10.20.30.4",
                    "10.20.30.40",
                    "10000.20000.300000000.400000000000000",
                };
            bool expected = true;
            bool actual;
            foreach (string contents in contentsPass)
            {
                actual =Version.ValidateVersion(contents);
                Assert.AreEqual(expected, actual, contents);
            }

            string[] contentsFail = {
                    "a.0.0.0",
                    "0.b.0.0",
                    "0.0.c.0",
                    "0.0.0.d",
                    "1.4",
                    "10.2.3.4.87",
                    "blah blah",
                    "",
                    null,
                    "!@#^%!*#@($QJVW4.!@$(^T!@#",
                };
            expected = false;
            foreach (string contents in contentsFail)
            {
                actual = Version.ValidateVersion(contents);
                Assert.AreEqual(expected, actual, contents);
            }
        }



        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
