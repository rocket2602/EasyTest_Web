﻿using EasyTest.Entity;
using Repository.Pattern.Infrastructure;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyTest.Service
{
    public interface ILogInService : IService<LogInSingleton>
    {
        bool IsUserLoggedOnElseWhere(Int32 userId,  String mac);
        bool IsYourLoginStillTrue(Int32 userId, String sid);
       
        void KillOtherSession(Int32 userId,String mac);
        void KillCurrentSession(String sid);
    }

    public class LogInService : Service<LogInSingleton>, ILogInService
    {
        private readonly IRepositoryAsync<LogInSingleton> _repository;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public LogInService(IRepositoryAsync<LogInSingleton> repository, IUnitOfWorkAsync unitOfWork)
            : base(repository)
        {
            _repository = repository;
            _unitOfWorkAsync = unitOfWork;
        }

        public bool IsYourLoginStillTrue(Int32 userId, string sid)
        {
            var logins = _repository.Query(x => x.LoggedIn == true && x.UserID == userId && x.SessionID==sid)
                                    .Select();

            return logins.Any();
        }

        public bool IsUserLoggedOnElseWhere(Int32 userId, String mac)
        {
            var logins = _repository.Query(x => x.LoggedIn == true && x.UserID == userId && x.MAC != mac)
                                    .Select();
            return logins.Any();
        }



        public void KillOtherSession(Int32 userId,String mac)
        {
            var logins = _repository.Query(x => x.LoggedIn == true && x.UserID == userId && x.MAC !=mac) 
                                     .Select();

            foreach (var item in logins)
            {
                item.LoggedIn = false;
                item.ObjectState = ObjectState.Modified;
                _repository.Update(item);
            }

            _unitOfWorkAsync.SaveChanges();
        }



        public void KillCurrentSession(String sid)
        {
            var login = _repository.Query(x => x.SessionID == sid)
                                    .Select()
                                    .FirstOrDefault();

            if (login != null)
            {
                login.ObjectState = ObjectState.Deleted;
                _repository.Delete(login);
                _unitOfWorkAsync.SaveChanges();
            }
        }
    }
}
