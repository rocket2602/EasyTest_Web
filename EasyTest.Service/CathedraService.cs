﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using Repository.Pattern.Infrastructure;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyTest.Service
{

	public interface ICathedraService : IService<Cathedra>
	{

        IQueryable<UniversityIncludeViewModel<Cathedra>> GetTodoList(Int32 langID);

        Cathedra GetOriginal(Int32 ParrentId, Int32 LangId);

        Int32[] GetTranslated(Int32 parentId);

        IEnumerable<Cathedra> GetAll(Int32 langId);

        IQueryable<UniversityIncludeViewModel<Cathedra>> GetCathedra(Int32? UniversityID, Int32? DepartamentID, Int32 langID);
    }

	public class CathedraService : Service<Cathedra>, ICathedraService
	{
		private readonly IRepositoryAsync<Cathedra> _repository;

		public CathedraService(IRepositoryAsync<Cathedra> repository)
			: base(repository)
		{
			_repository = repository;
		}

		public Cathedra GetOriginal(Int32 ParrentID, Int32 LangID)
		{
            var model = _repository.Query(x => x.ParentId == ParrentID && x.LanguageId == LangID)
                                  .Select();

            return model.FirstOrDefault();

		}

		public IQueryable<UniversityIncludeViewModel<Cathedra>> GetTodoList(Int32 langID)
        {
			var items = _repository.Queryable()
									.GroupBy(x => x.ParentId);

            var model = new List<UniversityIncludeViewModel<Cathedra>>();

            foreach (var group in items)
            {
                var vm = new UniversityIncludeViewModel<Cathedra>();

                var cathedra = group.Where(x => x.LanguageId == langID)
                                    .FirstOrDefault();

                vm.Entity = cathedra ?? group.First();

                vm.Status = cathedra != null;

                vm.Translated = group.Select(x => x.LanguageId).ToArray();

                model.Add(vm);
            }

            return model.AsQueryable();
        }

		public Int32[] GetTranslated(int parentID)
		{
			var data = _repository.Query(x => x.ParentId == parentID).Select(m => m.LanguageId);

            return data.ToArray();
		}

        public IQueryable<UniversityIncludeViewModel<Cathedra>> GetCathedra(Int32? UniversityID, Int32? DepartamentID, Int32 langID)
        {
            var array =  _repository.Query(x => x.DepartamentsCathedras.Any(c => c.UniversityId == UniversityID && c.DepartamentId==DepartamentID))
                                         .Select(x=>x.ParentId).ToArray();


            var model = new List<UniversityIncludeViewModel<Cathedra>>();

            var items = _repository.Query(x => array.Contains(x.ParentId)).Select();


            foreach (var group in items.GroupBy(x => x.ParentId))
            {
                var vm = new UniversityIncludeViewModel<Cathedra>();

                var cathedra = group.Where(x => x.LanguageId == langID).FirstOrDefault();

                vm.Entity = cathedra ?? group.First();

                vm.Status = cathedra != null;

                vm.Translated = group.Select(x => x.LanguageId).ToArray();

                model.Add(vm);
            }         

            return model.AsQueryable();
        }

        public IEnumerable<Cathedra> GetAll(Int32 langId)
        {
            var items = _repository.Queryable()
                                   .GroupBy(x => x.ParentId);

            var model = new List<Cathedra>();

            foreach (var group in items)
            {
                var vm = new UniversityIncludeViewModel<Cathedra>();


                var cathedra = group.Where(x => x.LanguageId == langId)
                                             .FirstOrDefault();

                model.Add(cathedra ?? group.First());

            }

            return model.AsEnumerable();

        }


    }
}
