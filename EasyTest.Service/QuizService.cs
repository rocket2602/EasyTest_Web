﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using Repository.Pattern.Infrastructure;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyTest.Service
{
    public interface IQuizService : IService<Quiz>
    {
    }


    public class QuizService : Service<Quiz>, IQuizService
    {
        private readonly IRepositoryAsync<Quiz> _repository;

        public QuizService(IRepositoryAsync<Quiz> repository)
            : base(repository)
        {
            _repository = repository;
        }

       
    }
}
