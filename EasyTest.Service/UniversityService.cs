﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using Repository.Pattern.Infrastructure;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace EasyTest.Service
{

    public interface IUniversityService : IService<University>
    {

        IEnumerable<UniversityViewModel> GetList(Int32? countryID, Int32 langID);


        IQueryable<UniversityViewModel> GetTodoList(Int32 langID);

        Int32[] GetTranslatedLanguages(Int32 id);
        Task<UniversitiesLang> GetUniversitiesLang(Int32 id, Int32 langId);

        UniversityViewModel FindById(Int32 id, Int32 langId);

    }

    public class UniversityService : Service<University>, IUniversityService
    {

        private readonly IRepositoryAsync<University> _repository;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public UniversityService(IRepositoryAsync<University> repository, IUnitOfWorkAsync unitOfWorkAsync)
            : base(repository)
        {
            _repository = repository;
            _unitOfWorkAsync = unitOfWorkAsync;
        }


        public UniversityViewModel FindById(Int32 id, Int32 langId)
        {
            var model = _repository.Query(x => x.Id == id)
                                    .Include(i => i.UniversitiesLangs)
                                    .Select(s => new UniversityViewModel()
                                    {
                                        Id = s.Id,
                                        CountryId = s.CountryId,
                                        TimeCreated = s.TimeCreated,
                                        LMSID = s.LMSID,
                                        Status = s.Status,
                                        LMS = s.LMS,
                                        Domain = s.Domain,
                                        Country = s.Country,
                                        UniversitiesLang = s.UniversitiesLangs.FirstOrDefault(x => x.LanguageId == langId)
                                    });

            return model.FirstOrDefault();
        }



        public IEnumerable<UniversityViewModel> GetList(Int32? countryID, Int32 langID)
        {
            var items = _repository.Query(x => x.Status == true && x.CountryId == countryID)
                                          .Include(i => i.UniversitiesLangs)
                                          .Select(m =>
                                  new UniversityViewModel()
                                  {
                                      //ID = m.ID,
                                      //LanguageID = langID,
                                      //CountryID = m.CountryID,
                                      //Name = m.UniversitiesLangs.Where(x => x.LanguageID == langID).FirstOrDefault().Name,
                                      //City = m.UniversitiesLangs.Where(x => x.LanguageID == langID).FirstOrDefault().City
                                  });//.OrderBy(x => x.Name);

            return items;
        }


        public async Task<UniversitiesLang> GetUniversitiesLang(Int32 id, Int32 langId)
        {
            var universityLang = await _unitOfWorkAsync.RepositoryAsync<UniversitiesLang>()
                                                  .Query(x => x.UniversityId == id && x.LanguageId == langId)
                                                  .SelectAsync();

            return universityLang.FirstOrDefault();
        }

        public IQueryable<UniversityViewModel> GetTodoList(Int32 langID)
        {

            var model = _repository.Query()
                                  .Include(i => i.UniversitiesLangs)
                                  .Include(i => i.Country)
                                  .Include(i => i.LMSID)
                                  .Select(
                                          m =>
                                              new UniversityViewModel()
                                              {
                                                  Id = m.Id,
                                                  CountryId = m.CountryId,
                                                  TimeCreated = m.TimeCreated,
                                                  LMSID = m.LMSID,
                                                  Status = m.Status,
                                                  LMS = m.LMS,
                                                  Domain = m.Domain,
                                                  Country = m.Country,
                                                  UniversitiesLang = m.UniversitiesLangs.Where(x => x.LanguageId == langID).FirstOrDefault()??m.UniversitiesLangs.FirstOrDefault(),
                                                  Translated = m.UniversitiesLangs.Select(x => x.LanguageId).ToList(),
                                              }
                                      );

            return model.AsQueryable();
        }



        public Int32[] GetTranslatedLanguages(Int32 id)
        {
            var data = _unitOfWorkAsync.RepositoryAsync<UniversitiesLang>()
                                        .Query(x => x.UniversityId == id)
                                        .Select(x => x.LanguageId)
                                        .ToArray();


            return data;
        }



    }

}
