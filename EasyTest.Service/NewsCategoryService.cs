﻿using EasyTest.Entity;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyTest.BL.Helpers;
using EasyTest.Entity.ViewModel;

namespace EasyTest.Service
{
    public interface INewsCategoryService : IService<NewsCategory>
    {
        IEnumerable<NewsCategoryViewModel> GetCategoriesMenu();
    }


    public class NewsCategoryService : Service<NewsCategory>,INewsCategoryService
    {
        private readonly IRepositoryAsync<NewsCategory> _NewsCategoryRepository;

        public NewsCategoryService(IRepositoryAsync<NewsCategory> NewsCategoryRepository) :base(NewsCategoryRepository)
        {
            _NewsCategoryRepository = NewsCategoryRepository;
        }

        public IEnumerable<NewsCategoryViewModel> GetCategoriesMenu()
        {

            var timeNow = DateTime.Now.ToUnixTime();

            var category = _NewsCategoryRepository.Query()
                                           .Include(i => i.News)
                                           .Select(s => new NewsCategoryViewModel()
                                           {
                                               Categories = s,
                                               NewsCount = s.News.Where(x => x.TimeCreated <= timeNow).Count(),
                                           }).ToList();


            category.ForEach(x => x.Categories.Name = Resource.Extend.GetString(x.Categories.Name));

            return category.AsEnumerable();
                                          
        }
    }
}
