﻿using EasyTest.Entity;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.Service
{
    public interface ILanguageService
    {
        Language GetCurrentLanguage(string culture);
        List<Language> GetSupported();
		List<String> GetAllLanguageCode();
    }
    public class LanguageService : Service<Language>, ILanguageService
    {

        private readonly IRepositoryAsync<Language> _repository;
        public LanguageService(IRepositoryAsync<Language> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public Language GetCurrentLanguage(String culture)
        {
            var language = _repository.Query(x => x.Code == culture).Select().FirstOrDefault();
            if (language == null) throw new ArgumentException();
            return language;
        }

        public List<Language> GetSupported()
        {
            var lanquages = _repository.Queryable();
            return lanquages.ToList();
        }


		public List<String> GetAllLanguageCode()
		{
			var languages = _repository.Query().Select(m=>m.Code).ToList();
			return languages;
		}



    }
}
