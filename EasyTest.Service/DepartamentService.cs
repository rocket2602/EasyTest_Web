﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using Repository.Pattern.Infrastructure;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyTest.Service
{

    public interface IDepartamentService : IService<Departament>
    {


        IQueryable<UniversityIncludeViewModel<Departament>> GetTodoList(Int32 langID);
        Departament GetOriginal(Int32 ParrentId, Int32 LangId);
        Int32[] GetTranslated(Int32 parentID);

        IEnumerable<Departament> GetAll(Int32 langId);

        IQueryable<UniversityIncludeViewModel<Departament>> GetDepartamentsByUniversityId(Int32 UniversityID, Int32 langID);
    }

    public class DepartamentService : Service<Departament>, IDepartamentService
    {


        private readonly IRepositoryAsync<Departament> _repository;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public DepartamentService(IRepositoryAsync<Departament> repository, IUnitOfWorkAsync unitOfWorkAsync)
            : base(repository)
        {
            _repository = repository;
            _unitOfWorkAsync = unitOfWorkAsync;
        }




        public Departament GetOriginal(Int32 ParrentID, Int32 LangID)
        {
            var model = _repository.Query(x => x.ParentId == ParrentID && x.LanguageId == LangID)
                                   .Select();

            return model.FirstOrDefault();
        }

        public IQueryable<UniversityIncludeViewModel<Departament>> GetTodoList(Int32 langID)
        {
            var items = _repository.Queryable()
                                    .GroupBy(x => x.ParentId);

            var model = new List<UniversityIncludeViewModel<Departament>>();

            foreach (var group in items)
            {
                var vm = new UniversityIncludeViewModel<Departament>();


                var departament = group.Where(x => x.LanguageId == langID)
                                             .FirstOrDefault();

                vm.Entity = departament ?? group.First();

                vm.Status = departament != null;

                vm.Translated = group.Select(x => x.LanguageId).ToArray();

                model.Add(vm);
            }

            return model.AsQueryable();
        }

        public Int32[] GetTranslated(int parentID)
        {
            var data = _repository.Query(x => x.ParentId == parentID).Select(m => m.LanguageId);

            return data.ToArray();
        }


        public IEnumerable<Departament> GetAll(Int32 langId)
        {
                                 
            var model = new List<Departament>();

            foreach (var group in _repository.Queryable().GroupBy(x => x.ParentId))
            {
                var departament = group.Where(x => x.LanguageId == langId)
                                             .FirstOrDefault();

                model.Add(departament ?? group.First());
            }

            return model.AsEnumerable();

        }

        public  IQueryable<UniversityIncludeViewModel<Departament>> GetDepartamentsByUniversityId(Int32 UniversityID, Int32 langID)
        {
            var array = _repository.Query(x => x.Universities.Any(d => d.Id == UniversityID))
                                   .Select(x => x.ParentId).ToArray();

            var model = new List<UniversityIncludeViewModel<Departament>>();
          
            var items = _repository.Query(x => array.Contains(x.ParentId)).Select();


            foreach (var group in items.GroupBy(x => x.ParentId))
            {
                var vm = new UniversityIncludeViewModel<Departament>();

                var departament = group.Where(x => x.LanguageId == langID).FirstOrDefault();

                vm.Entity = departament ?? group.First();

                vm.Status = departament != null;

                vm.Translated = group.Select(x => x.LanguageId).ToArray();

                model.Add(vm);
            }

            return model.AsQueryable();
        }
    }
}
