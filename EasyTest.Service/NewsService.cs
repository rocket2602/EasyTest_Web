﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using Repository.Pattern.Infrastructure;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyTest.BL.Helpers;

namespace EasyTest.Service
{

    public interface INewsService : IService<News>
    {
        IEnumerable<NewsCategory> GetNewsCategories();

        IQueryable<NewsViewModel> GetAll(Int32 langID);


        IQueryable<NewsViewModel> GetTodoList(Int32 langID);

        NewsViewModel FindNewsByID(Int32 id, Int32 langID, Boolean OnlyPast = true);

        IEnumerable<NewsViewModel> GetAllAdmin(Int32 langID);

        Dictionary<String, Int32> GetNewsArchieveLinks();
        IQueryable<NewsViewModel> GetAllByDateTime(long startDate, long finishDate, int langID);
        Int32[] GetTranslated(Int32 id);

        NewsLang GetNewsLang(Int32? id, Int32? langId);
    }

    public class NewsService : Service<News>, INewsService
    {
        private readonly IRepositoryAsync<News> _newsRepository;
        private readonly IRepositoryAsync<NewsLang> _newsLangRepository;

        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public NewsService(IRepositoryAsync<News> repository, IRepositoryAsync<NewsLang> newslangRepository, IUnitOfWorkAsync unitOfWork)
            : base(repository)
        {
            _newsRepository = repository;
            _unitOfWorkAsync = unitOfWork;
            _newsLangRepository = newslangRepository;
        }

        public IQueryable<NewsViewModel> GetTodoList(Int32 langID)
        {
            var news = _newsRepository.Query()
                                 .Include(i => i.NewsLangs)
                                 .Include(i => i.NewsCategory)
                                 .Include(i => i.UsersInfo)
                                 .Select(s => new NewsViewModel()
                                 {
                                     News = s,
                                     NewsLang = s.NewsLangs.FirstOrDefault(x => x.LanguageId == langID),
                                     Translated = s.NewsLangs.Select(x => x.LanguageId).ToList(),
                                     NewsCategory = s.NewsCategory,
                                     UserInfo = s.UsersInfo

                                 });

            return news.AsQueryable();
        }

        public IEnumerable<NewsCategory> GetNewsCategories()
        {
            var categories = _unitOfWorkAsync.RepositoryAsync<NewsCategory>()
                                            .Queryable()
                                            .ToList();

            foreach (var item in categories)
            {
                item.Name = Resource.Extend.GetString(item.Name);
            }

            return categories;
        }

        public IQueryable<NewsViewModel> GetAll(Int32 langID)
        {
            var timeNow = DateTime.Now.ToUnixTime();

            var news = _newsRepository.Query(x => x.TimeCreated <= timeNow)
                                  .Include(i => i.NewsLangs)
                                  .Include(i => i.NewsCategory)
                                  .Include(i => i.UsersInfo)
                                   .OrderBy(q => q.OrderByDescending(d => d.TimeCreated))
                                  .Select(s => new NewsViewModel()
                                  {
                                      News = s,
                                      NewsLang = s.NewsLangs.FirstOrDefault(x => x.LanguageId == langID),
                                      NewsCategory = s.NewsCategory,
                                      UserInfo = s.UsersInfo
                                  });



            return news.AsQueryable();
        }

        //public IEnumerable<NewsViewModel> GetAllAdmin(Int32 langID)
        //{


        //    var news = _repository.Query()
        //                          .Include(i => i.NewsLangs)
        //                          .Include(i => i.NewsCategory)
        //                          .OrderBy(q => q.OrderByDescending(d => d.DateTime))
        //                          .Select(s => new NewsViewModel()
        //                          {
        //                              ID = s.ID,
        //                              Image = s.Image,
        //                              Views = s.Views,
        //                              AuthorID = s.AuthorID,
        //                              DateTime = s.DateTime,
        //                              CategoryID = s.CategoryID,
        //                              Name = s.NewsLangs.Where(x => x.LanguageID == langID).FirstOrDefault().Name,
        //                              Translated = s.NewsLangs.Select(x => x.LanguageID).ToList()
        //                          });





        //    return news;
        // }


        public Dictionary<String, Int32> GetNewsArchieveLinks()
        {
            var dt = DateTime.Now.ToUnixTime();

            var archieve = _newsRepository.Query(x => x.TimeCreated <= dt)
                                             .Select()
                                             .GroupBy(x => x.TimeCreated.ToDateTime().ToString("MMMM yyyy"))
                                             .ToDictionary(x => x.Key, x => x.Count());

            return archieve;
        }

        public NewsViewModel FindNewsByID(Int32 id, Int32 langId, Boolean OnlyTimeCreadedPast = true)
        {
            var newsVM = _newsRepository.Query(x => x.Id == id)
                                  .Include(i => i.NewsLangs)
                                  .Include(i => i.NewsCategory)
                                  .Include(i => i.UsersInfo)
                                  .Select(s => new NewsViewModel()
                                  {
                                      NewsLang = s.NewsLangs.Where(x => x.LanguageId == langId).FirstOrDefault(),
                                      News = s,
                                      NewsCategory = s.NewsCategory,
                                      UserInfo = s.UsersInfo

                                  });

            return newsVM.FirstOrDefault();
        }

        public NewsLang GetNewsLang(Int32? id, Int32? langId)
        {
            var newsLang = _newsLangRepository.Query(x => x.NewsId == id && x.LanguageId == langId)
                                              .Select();

            return newsLang.FirstOrDefault();
        }


        public Int32[] GetTranslated(Int32 id)
        {
            var translated = _newsLangRepository.Query(x => x.NewsId == id)
                .Select(x => x.LanguageId).ToArray();

            return translated;
        }


        public IEnumerable<NewsViewModel> GetAllAdmin(int langID)
        {
            throw new NotImplementedException();
        }

        public IQueryable<NewsViewModel> GetAllByDateTime(long startDate, long finishDate, int langID)
        {
            var now = DateTime.Now.ToUnixTime();

            var news = _newsRepository.Query(x => x.TimeCreated <= now && x.TimeCreated >= startDate && x.TimeCreated <= finishDate)
                                        .Include(i => i.NewsLangs)
                                        .Include(i => i.NewsCategory)
                                        .Include(i => i.UsersInfo)
                                        .Select(s => new NewsViewModel()
                                        {
                                            News = s,
                                            NewsLang = s.NewsLangs.FirstOrDefault(x => x.LanguageId == langID),
                                            NewsCategory = s.NewsCategory,
                                            UserInfo = s.UsersInfo

                                        }).OrderBy(x => x.News.TimeCreated);

            return news.AsQueryable();
        }
    }
}
