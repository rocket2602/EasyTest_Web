﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using System;
using System.Collections.Generic;


namespace EasyTest.Service
{
    public interface ICountryService:IService<Country>
    {   
        IEnumerable<Country> GetAll(int LangID);
    }

    public class CountryService : Service<Country>, ICountryService
    {
        private readonly IRepositoryAsync<Country> _repository;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        public CountryService(IRepositoryAsync<Country> repository, IUnitOfWorkAsync unitOfWorkAsync)
            :base(repository)
        {
            _repository = repository;
            _unitOfWorkAsync = unitOfWorkAsync;
        }


        public IEnumerable<Country> GetAll(Int32 LangID)
        {
			var countries = _repository.Queryable();
            return countries;
        }


   


    }
}
