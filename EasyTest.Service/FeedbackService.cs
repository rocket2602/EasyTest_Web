﻿using EasyTest.Entity;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.Service
{
    public class FeedbackService:Service<Feedback>
    {
        private readonly IRepositoryAsync<Feedback> _repository;
        public FeedbackService(IRepositoryAsync<Feedback> repository)
            :base(repository)
        {
            _repository = repository;
        }




    }
}
