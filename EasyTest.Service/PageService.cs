﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyTest.Service
{

    public interface IPageService : IService<Page>
    {

       PageViewModel FindPageByID(Int32 id,Int32 langID);
    }

    public class PageService : Service<Page>,IPageService
    {
        private readonly IRepositoryAsync<Page> _repository;

        public PageService(IRepositoryAsync<Page> repository)
			: base(repository)
		{
			_repository = repository;
		}



        public PageViewModel FindPageByID(Int32 id, Int32 langID)
        {
            var page = _repository.Query(x => x.ID == id)
                                  .Include(i => i.PageLangs)
                                  .Select(m => new PageViewModel() { 
                                    PageID = m.ID,
                                    Title = m.Title,
                                    Name = m.PageLangs.FirstOrDefault().Name,
                                    Description = m.PageLangs.FirstOrDefault().Description,
                                    Keywords = m.PageLangs.FirstOrDefault().Keywords,
                                    Text = m.PageLangs.FirstOrDefault().Text
                                  })
                                  .FirstOrDefault();

            return page;
        }
    }
}
