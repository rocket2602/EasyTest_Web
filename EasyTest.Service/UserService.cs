﻿using EasyTest.Entity;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.Service
{
    public interface IUserService : IService<User>
    {
        User FindUser(String email, String password);
    }


    public class UserService : Service<User>, IUserService
    {
        private readonly IRepositoryAsync<User> _repository;

        public UserService(IRepositoryAsync<User> repository)
            : base(repository)
        {
            _repository = repository;
        }


        public User FindUser(String email, String password)
        {

            var user = _repository.Query(x => x.Email == email && x.Password == password)
                                  .Select()
                                  .SingleOrDefault();

            return user;

        }



    }
}
