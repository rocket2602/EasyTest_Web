﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using Repository.Pattern.Infrastructure;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyTest.BL.Quiz;

namespace EasyTest.Service
{

    public interface IQuizQuestionService : IService<QuizQuestion>
    {
         long[] GetUniqueQuestionsId(long[] qid, Int32 universityId);
        IQueryable<QuestionsType> GetQuestionsType();

        IEnumerable<QuizQuestion> GetQuestionsWithAnswers(Int32 tid,long[] qid);
    }

    public class QuizQuestionService : Service<QuizQuestion>, IQuizQuestionService
    {
        private readonly IRepositoryAsync<QuizQuestion> _repository;

        private readonly IUnitOfWorkAsync _unitOfWork;


        public QuizQuestionService(IRepositoryAsync<QuizQuestion> repository, IUnitOfWorkAsync unitOfWork)
            :base(repository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public IQueryable<QuestionsType> GetQuestionsType()
        {
            var qtype = _unitOfWork.RepositoryAsync<QuestionsType>().Queryable();

            return qtype;
        }

        public  long[] GetUniqueQuestionsId(long[] qid,Int32 universityId)
        {
            var data =  _repository.Query(x =>x.Quiz.UniversityId== universityId && qid.Contains(x.ParentId.Value))
                                    .Select(x=>x.ParentId);

            var unique = qid.Where(x=>!data.Contains(x))
                            .Distinct()
                            .ToArray();

            return unique;
        }

        


        public IEnumerable<QuizQuestion> GetQuestionsWithAnswers(Int32 tid,long[] qid)
        {
            var questions = _repository.Query(x =>x.QuizId==tid && qid.Contains(x.ParentId.Value))
                                        .Include(i=>i.QuizAnswers)
                                        .Include(i => i.QuizMultiQuestions)
                                        .Include(i=>i.QuestionsType)
                                        //.Include(i=>i.MoodleMatchSubquestions)
                                        //.Include(i=>i.UsersAnswers)
                                        .Select();

            return questions;
        }
    }
}
