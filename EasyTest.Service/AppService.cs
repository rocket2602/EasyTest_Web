﻿using EasyTest.Entity;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyTest.Entity.ViewModel;
using EasyTest.BL.Helpers;

namespace EasyTest.Service
{
    public interface IAppService : IService<App>
    {
        AppViewModel GetById(Int32 id, Int32 langID);
        IQueryable<AppViewModel> GetTodoList(int langID);
        Int32[] GetTranslated(Int32 id);
        AppChanges GetAppChange(int id, int cultureFrom);
        IQueryable<AppViewModel> GetAll(int langID);
        AppViewModel FindById(int? id, int langID);
    }

    public class AppService : Service<App>, IAppService
    {
        private readonly IRepositoryAsync<App> _appRepository;

        private readonly IRepositoryAsync<AppChanges> _appChageRepository;

        public AppService(IRepositoryAsync<App> repository, IRepositoryAsync<AppChanges> appChageRepository)
            : base(repository)
        {
            _appRepository = repository;
            _appChageRepository = appChageRepository;
        }

        public AppViewModel GetById(Int32 id, Int32 langID)
        {
            var app = _appRepository.Query(x => x.Id == id)
                                  .Include(i => i.AppChanges)
                                  .Select(s => new AppViewModel()
                                  {
                                      Entity = s,
                                      AppChange = s.AppChanges.FirstOrDefault(x => x.LanguageId == langID)
                                  })
                                  .FirstOrDefault();

            return app;
        }

        public IQueryable<AppViewModel> GetTodoList(int langID)
        {
            var app = _appRepository.Query().Include(i => i.AppChanges).Select(s => new AppViewModel()
            {
                Entity = s,
                AppChange = s.AppChanges.FirstOrDefault(x => x.LanguageId == langID),
                Translated = s.AppChanges.Select(x => x.LanguageId).ToList()
            });


            return app.AsQueryable();

        }

        public int[] GetTranslated(int id)
        {
            var translated = _appChageRepository.Query(x => x.AppId == id)
                                                .Select(x => x.LanguageId)
                                                .ToArray();

            return translated;
        }

        public AppChanges GetAppChange(int id, int cultureFrom)
        {
            var AppChanges = _appChageRepository.Query(x => x.AppId == id && x.LanguageId == cultureFrom)
                                              .Select();

            return AppChanges.FirstOrDefault();
        }

        public IQueryable<AppViewModel> GetAll(int langID)
        {
            var timeNow = DateTime.Now.ToUnixTime();

            var model = _appRepository.Query(x => x.TimeCreated <= timeNow).Select(s => new AppViewModel()
            {
                Entity = s,
                AppChange = s.AppChanges.FirstOrDefault(x => x.LanguageId == langID)
            });

            return model.AsQueryable();

        }

        public AppViewModel FindById(int? id, int langID)
        {
            var timeNow = DateTime.Now.ToUnixTime();

            var model = _appRepository.Query(x => x.TimeCreated <= timeNow && x.Id == id).Select(s => new AppViewModel()
            {
                Entity = s,
                AppChange = s.AppChanges.FirstOrDefault(x => x.LanguageId == langID)
            });

            return model.SingleOrDefault();

        }
    }
}
