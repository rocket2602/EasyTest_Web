﻿using EasyTest.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class NewsCategoryController : Controller
    {

        private readonly INewsCategoryService _newsCategoryService;

        public NewsCategoryController(INewsCategoryService service)
        {
            _newsCategoryService = service;
        }

        [ChildActionOnly]
        public ActionResult Index()
        {
            var vm = _newsCategoryService.GetCategoriesMenu();

            return PartialView(vm);
        }
    }
}