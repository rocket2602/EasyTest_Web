﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Web.Mvc;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    public class CaptchaController : Controller
    {
        
        public ActionResult ShowCaptcha()
        {
            return PartialView("_Captcha");

        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public FileContentResult CaptchaImage()
        {
        
            if (Session["request"] != null)
            {
                Session.Remove("request");
            }

            var memStream = new MemoryStream();
            Int32 res;
            String text = CaptchaHelper.GetCaptcha(out res);

            Session["Captcha"] = res;//Запис результату в сесію

            FileContentResult img = null;

            //Generate an image from the text stored in session
            Bitmap imgCapthca = CaptchaHelper.GenerateImage(180, 30, text);

            imgCapthca.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);

            byte[] imgBytes = memStream.GetBuffer();
            imgCapthca.Dispose();
            memStream.Close();
            img = this.File(memStream.GetBuffer(), "image/Jpeg");
            return img;
        }

      
        [HttpPost]
        public ActionResult HelpMe()
        {
            if (Session["request"] != null && (Boolean)Session["request"] == true)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session["request"] = true;
            Int32 value = (Int32)Session["Captcha"];
           
            var helpList =   CaptchaHelper.HelpMe(value);
           
            return Json(helpList);
        }
    }
}