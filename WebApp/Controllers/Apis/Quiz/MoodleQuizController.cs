﻿using EasyTest.BL.Helpers;
using EasyTest.BL.Quiz.Model;
using EasyTest.Service;
using Repository.Pattern.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebApp.Models;
using WebApp.Models.Quiz;
using entity = EasyTest.Entity;
using EasyTest.Entity;

namespace WebApp.Controllers.Apis.Quiz
{
    [RoutePrefix("api/moodlequiz")]
    public class MoodleQuizController : QuizController
    {
        public MoodleQuizController(IQuizService quizService, IQuizQuestionService quizQuestionService)
            : base(quizService, quizQuestionService)
        { }

        [HttpPost]
        [Route("saveunique")]
        public async Task<IHttpActionResult> SaveUnique(QuizBindingModel<MoodleQuizQuestion> model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);


            var user = await GetUserInfoAsync();


            var quiz = (await _quizService.Query(x => x.UniversityId == user.UniversityId && x.ParentId == model.tid)
                                           .SelectAsync())
                                           .SingleOrDefault();

            Int32 quizId = (quiz == null) ? await AddNewQuiz(model.tid, model.QuizName, LMSType.Moodle) : quiz.Id;

            var uniqueQuestion = GetOnlyUniqueQuestion(model.Questions);

            PrepareQuestionsToInsert(uniqueQuestion, quizId);

            await SaveChangesAsync();

            return Ok("Succsessfuly Inserted!!!");
        }

        private List<MoodleQuizQuestion> GetOnlyUniqueQuestion(List<MoodleQuizQuestion> questions)
        {
            //todo
            return questions;
        }

        private void PrepareQuestionsToInsert(List<MoodleQuizQuestion> questions, int quizId)
        {
            var userId = 13;//User.Identity.GetUserId<Int32>();

            var qtype = _quizQuestionService.GetQuestionsType();

            var data = new List<entity.QuizQuestion>();

            foreach (var item in questions)
            {
                entity.QuizQuestion question;

                QuizAnswer answer;


                if (item.Type == MoodleQuestionType.randomsamatch)
                {
                    answer = new QuizAnswer()
                    {
                        Name = String.Join("|", item.Answers),
                        ObjectState = ObjectState.Added
                    };

                    foreach (var q in item.MultiQuestions)
                    {
                        question = new entity.QuizQuestion()
                        {
                            Name = item.Question,
                            UserId = userId,
                            QuizId = quizId,
                            Type = GetQuestionType(qtype, MoodleQuestionType.shortanswer),
                            TimeCreated = DateTime.Now.ToUnixTime(),
                            ObjectState = ObjectState.Added
                        };
                        question.QuizAnswers.Add(answer);
                    }

                    continue;
                }

                question = new entity.QuizQuestion()
                {
                    Name = item.Question,
                    UserId = userId,
                    QuizId = quizId,
                    Type = GetQuestionType(qtype, item.Type),
                    TimeCreated = DateTime.Now.ToUnixTime(),
                    ObjectState = ObjectState.Added
                };

                if (item.Type == MoodleQuestionType.match)
                {
                    var answ = String.Join("|", item.Answers);

                    foreach (var mq in item.MultiQuestions)
                    {
                        var moodleMatch = new MoodleMatchSubquestion()
                        {
                            Name = mq,
                            Answer = answ,
                            ObjectState = ObjectState.Added
                        };

                        question.MoodleMatchSubquestions.Add(moodleMatch);
                    }
                    data.Add(question);
                    continue;
                }
          
                switch (item.Type)
                {
                    case MoodleQuestionType.truefalse:
                    case MoodleQuestionType.multichoice:
                    case MoodleQuestionType.multisubanswers:

                        foreach (var answ in item.Answers)
                        {
                            answer = new QuizAnswer()
                            {
                                Name = answ,
                                ObjectState = ObjectState.Added
                            };

                            question.QuizAnswers.Add(answer);
                        }

                        break;
                }

                data.Add(question);
            }
            _quizQuestionService.InsertGraphRange(data);
        }
        private Int16 GetQuestionType(IQueryable<QuestionsType> qtype, MoodleQuestionType type)
        {
            var item = qtype.Where(x => x.Name == type.ToString())
                            .Select(x => x.Id)
                            .FirstOrDefault();

            if (item == 0)
                throw new Exception("Question Type not found");

            return item;


        }


    }
}
