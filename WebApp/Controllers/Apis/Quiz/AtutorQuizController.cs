﻿using EasyTest.BL.Helpers;
using EasyTest.Service;
using Repository.Pattern.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using EasyTest.Entity;
using WebApp.Models;
using System.Web.Http.ModelBinding;
using WebApp.Models.Quiz;
using EasyTest.BL.Quiz.Model;

namespace WebApp.Controllers.Apis.Quiz
{
    [RoutePrefix("api/atutorquiz")]
    public class AtutorQuizController : QuizController
    {

        public AtutorQuizController(IQuizService quizService, IQuizQuestionService quizQuestionService)
            : base(quizService, quizQuestionService)
        { }


       [Route("getanswers")]
        public IEnumerable<QuizQuestion> Get(Int32 id, [ModelBinder]long[] qid)
        {
            var questions = _quizQuestionService.GetQuestionsWithAnswers(id,qid);

            return questions.ToList();

        }


        [Route("getunique")]
        public long[] GetUnique([ModelBinder]long[] qid)
        {

            var user = GetUserInfo();

            var unique = _quizQuestionService.GetUniqueQuestionsId(qid, user.UniversityId);

            return unique;
        }

        [HttpPost]
        [Route("saveunique")]
        public async Task<IHttpActionResult> SaveUnique(QuizBindingModel<AtutorQuizQuestion> model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var quiz = (await _quizService.Query(x => x.LMSId == (int)LMSType.Atutor && x.ParentId == model.tid)
                                           .SelectAsync())
                                           .SingleOrDefault();

            Int32 quizId = (quiz == null) ? await AddNewQuiz(model.tid, model.QuizName, LMSType.Atutor) : quiz.Id;



            try
            {

                PrepareQuestionsToInsert(model.Questions, quizId);
            }
            catch (Exception e)
            {
                
            }

            await SaveChangesAsync();

            return Ok<Int32>(quizId);
        }

        private void PrepareQuestionsToInsert(List<AtutorQuizQuestion> questions, int quizId)
        {
            var userId = 13;//User.Identity.GetUserId<Int32>();

            var qtype = _quizQuestionService.GetQuestionsType();

            var data = new List<QuizQuestion>();

            foreach (var item in questions)
            {
                var question = new QuizQuestion()
                {
                    Name = item.Question,
                    UserId = userId,
                    QuizId = quizId,
                    Type = GetQuestionType(qtype, item.Type),
                    ParentId = item.qid,
                    TimeCreated = DateTime.Now.ToUnixTime(),
                    ObjectState = ObjectState.Added
                };

                if (item.Type == AtutorQuestionsType.matchingSimple || item.Type == AtutorQuestionsType.matchingGraphical)
                {
                    foreach (var mq in item.MultiQuestions)
                    {
                        var multiQuestion = new QuizMultiQuestion()
                        {
                            Name = mq,
                            ObjectState = ObjectState.Added
                        };

                        question.QuizMultiQuestions.Add(multiQuestion);
                    }
                }

                foreach (var answ in item.Answers)
                {
                    var answer = new QuizAnswer()
                    {
                        Name = answ,
                        ObjectState = ObjectState.Added
                    };

                    question.QuizAnswers.Add(answer);
                }

                data.Add(question);
            }
            _quizQuestionService.InsertGraphRange(data);
        }

        private Int16 GetQuestionType(IQueryable<QuestionsType> qtype, AtutorQuestionsType type)
        {
            var item = qtype.Where(x => x.Name == type.ToString())
                            .Select(x => x.Id)
                            .FirstOrDefault();

            if (item == 0)
                throw new Exception("Question Type not found");

            return item;


        }


    }
}
