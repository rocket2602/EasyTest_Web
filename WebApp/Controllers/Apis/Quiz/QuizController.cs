﻿using EasyTest.BL.Helpers;
using EasyTest.Service;
using Repository.Pattern.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebApp.Models;
using EasyTest.Entity;

namespace WebApp.Controllers.Apis.Quiz
{
    public class QuizController : BaseApiController
    {
        protected readonly IQuizService _quizService;

        protected readonly IQuizQuestionService _quizQuestionService;

        public QuizController(IQuizService quizService, IQuizQuestionService quizQuestionService)
        {
            _quizService = quizService;

            _quizQuestionService = quizQuestionService;
        }

        protected async Task<Int32> AddNewQuiz(Int32 tid, String quizName,LMSType lms)
        {
            var user = await GetUserInfoAsync();
           
            var quiz = new EasyTest.Entity.Quiz()
            {
                Name = quizName,
                ParentId = tid,
                LMSId = (Int32)lms,
                UserId = user.UserId,
                UniversityId = user.UniversityId,
                DepartamentId = user.DepartamentId,
                CathedraId = user.CathedraId,
                TimeCreated = DateTime.Now.ToUnixTime(),
                ObjectState = ObjectState.Added
            };

            _quizService.Insert(quiz);

            await SaveChangesAsync();

            return quiz.Id;
        }



       
    }
}
