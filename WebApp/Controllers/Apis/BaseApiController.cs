﻿using EasyTest.Entity;
using Ninject;
using Repository.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebApp.Controllers.Apis
{
    public abstract class BaseApiController : ApiController
    {
        [Inject]
        public IUnitOfWorkAsync _unitOfWorkAsync { get; set; }


        [NonAction]
        public async Task<UsersInfo> GetUserInfoAsync()
        {
            var userId = 13;//User.Identity.GetUserId<Int32>();

            var user = await _unitOfWorkAsync.RepositoryAsync<UsersInfo>().FindAsync(userId);

            return user;
        }

        [NonAction]
        public UsersInfo GetUserInfo()
        {
            var userId = 13;//User.Identity.GetUserId<Int32>();

            var user =  _unitOfWorkAsync.RepositoryAsync<UsersInfo>().Find(userId);

            return user;
        }

        [NonAction]
        public async Task SaveChangesAsync()
        {
            try
            {
                await _unitOfWorkAsync.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }
    }
}
