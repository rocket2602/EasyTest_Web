﻿using EasyTest.Entity;
using EasyTest.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApp.Controllers.Apis
{
    [Authorize]
    public class FirstController : BaseApiController
    {

        private readonly IUserService _userService;


        public FirstController(IUserService _userService)
        {
            this._userService = _userService;
        }



        public string Get()
        {
           // var users = _userService.Queryable();
           // return users;
            return "Hi from server";
        }


    }
}
