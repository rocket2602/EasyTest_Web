﻿using EasyTest.Entity;
using EasyTest.Service;
using Ninject;
using Repository.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    public abstract class BaseController : Controller
    {

        #region Fields

        public Int32 langID { get; private set; }

        public String CurrentLang { get; private set; }

        private Int32 pageSize, totalPageCount;

		private const String CookieName = "_culture";

        [Inject]
        public IUnitOfWorkAsync _unitOfWorkAsync { get; set; }

        #endregion

        #region UoF 
        [Inject]
        public IUserService _userService { get; set; }


    
        [Inject]
        public ICountryService _countryService { get; set; }


        [Inject]
        public ILanguageService _lanquageService { get; set; }

    

        [Inject]
        public IPageService _pageService { get; set; }

        [Inject]
        public ILogInService _logInService { get; set; }


		#endregion

		public Int32 PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        [OutputCache(Duration = 60)]
        protected List<Language> GetSupportedLanguages()
        {
            var languages = _lanquageService.GetSupported();
            return languages;
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await _unitOfWorkAsync.SaveChangesAsync();
            }
            catch (Exception e)
            {
                //log write todo
            }
        }


        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, Object state)
        {
            String cultureName = RouteData.Values["culture"] as String;

            // Attempt to read the culture cookie from Request
            if (cultureName == null)
                cultureName = GetSavedCultureOrDefault();

            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe


            if (RouteData.Values["culture"] as String != cultureName)
            {
                // Force a valid culture in the URL
                RouteData.Values["culture"] = cultureName.ToLowerInvariant(); // lower case too
                // Redirect user
                Response.RedirectToRoute(RouteData.Values);
            }

            SetCultureOnThread(cultureName);

            SaveCultureInCookie(cultureName);

            return base.BeginExecuteCore(callback, state);
        }
        private String GetSavedCultureOrDefault()
        {
            var cookie = Request.Cookies[CookieName];

            if (cookie != null)
                return cookie.Value;
            else
                return Request.UserLanguages != null && Request.UserLanguages.Length > 0 ? Request.UserLanguages[0] : null; // obtain it from HTTP header AcceptLanguages


        }
        private void SetCultureOnThread(String culture)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
			langID = CultureHelper.GetCultureID(culture);
			CurrentLang = culture;      
        }
        private void SaveCultureInCookie(String cultureName)
        {
            var _cookie = new HttpCookie(CookieName, cultureName);
            _cookie.Expires = DateTime.Now.AddYears(1);
            Response.SetCookie(_cookie);
        }

    }
}