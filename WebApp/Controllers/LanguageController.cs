﻿using EasyTest.Entity;
using EasyTest.Service;
using Repository.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebApp.Helpers;
using WebApp.Models;
using WevApp.Helpers;

namespace WebApp.Controllers
{
    public class LanguageController : BaseController
    {
        public ActionResult GetSupportLanguage()
        {
            var lanquage = GetSupportedLanguages();
            return PartialView(lanquage);
        }

        [NonAction]
        public List<String> GetAllCultureCode()
        {

            return _lanquageService.GetAllLanguageCode();
        }

        RouteData routeData;

        public ActionResult SetCulture(String culture)
        {
            culture = CultureHelper.GetImplementedCulture(culture);
            ChangeRouteCulture(culture);
            return RedirectToRoute(routeData.Values);
        }

        private void ChangeRouteCulture(string culture)
        {          
            var fullUrl = Request.UrlReferrer.ToString();
            var questionMarkIndex = fullUrl.IndexOf('?');
            string queryString = null;
            string url = fullUrl;
            if (questionMarkIndex != -1) // There is a QueryString
            {
                url = fullUrl.Substring(0, questionMarkIndex);
                queryString = fullUrl.Substring(questionMarkIndex + 1);
            }

            // Arranges
            var request = new HttpRequest(null, url, queryString);
            var response = new HttpResponse(new System.IO.StringWriter());
            var httpContext = new HttpContext(request, response);

            routeData = System.Web.Routing.RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            routeData.Values["culture"] = culture;
        }
    }
}