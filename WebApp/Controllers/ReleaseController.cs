﻿using EasyTest.Service;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class ReleaseController : BaseController
    {
        private readonly IAppService _appService; 

        public ReleaseController(IAppService appService)
        {
            _appService = appService;
        }

    

        public ActionResult All(Int32 page=1)
        {
            PageSize = 5;

            var apps = _appService.GetAll(langID).OrderBy(x=>x.Entity.TimeCreated);

            return View(apps.ToPagedList(page, PageSize));
        }

      
        public ActionResult Show(Int32? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var vm = _appService.FindById(id,langID);

            if (vm == null)
            {
                return HttpNotFound();
            }


            return View(vm);
        }

    }
}