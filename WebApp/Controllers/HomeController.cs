﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public enum PageName
    { 
        Index =1,
        About ,


        
        
    }




    public class HomeController : BaseController
    {
        public ActionResult Index()
        {

          
            return View();
        }



        public ActionResult JSDisabled()
        {
            return View();
        }

        public ActionResult About()
        {
            var model = _pageService.FindPageByID((Int32)PageName.About, langID);

            return PartialView(model);
        }
  

    
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
      
    }
}