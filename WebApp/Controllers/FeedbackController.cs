﻿using EasyTest.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class FeedbackController : BaseController
    {
        // GET: Feedback
        public ActionResult Index()
        {
            SubjectDropDownList();
            return View();
        }
        private void SubjectDropDownList()
        {

            var subject = _unitOfWorkAsync.Repository<FeedbackCategory>().Queryable();
            ViewBag.Subject = subject;
        }

    }
}