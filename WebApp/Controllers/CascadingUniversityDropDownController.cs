﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Threading.Tasks;
//using System.Web;
//using System.Web.Mvc;

//namespace WebApp.Controllers
//{
//    public class CascadingUniversityDropDownController : BaseController
//    {
 

//        [HttpPost]
//        public ActionResult SelectUniversity(Int32? CountryID)
//        {
//            if (CountryID == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);    
//            }

//            var university = _universityService.GetList(CountryID,langID);


//            return Json(new { success = true, data = university.Select(x => new {Value = x.ID,Text = x.Name + "("+ x.City + ")"  }) });
//        }


//        [HttpPost]
//        public async Task<ActionResult> SelectDepartament(Int32? UniversityID)
//        {
//            if (UniversityID == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var departament = await _departamentService.GetDepartamentsAsync(UniversityID.Value, langID);

//            return Json(new { success = true, data = departament.Select(x => new { Value = x.ID, Text = x.Name }) });
//        }

//        [HttpPost]
//        public async Task<ActionResult> SelectCathedra(Int32? UniversityID,  Int32? DepartamentID)
//        {
//            if (UniversityID ==null || DepartamentID == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var cathedra = await _cathedraService.GetCathedraAsync(UniversityID,DepartamentID,langID);

//            return Json(new { success = true, data = cathedra.Select(x => new { Value = x.ID, Text = x.Name }) });
//        }


//    }
//}