﻿using EasyTest.Entity;
using Repository.Pattern.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using PagedList;
using EasyTest.Service;
using System.Net;
using WevApp.Helpers;
using System.ServiceModel.Syndication;
using WebApp.Helpers;
using EasyTest.BL.Helpers;
using EasyTest.Entity.ViewModel;

namespace WebApp.Controllers
{
    public class NewsController : BaseController
    {
        private const String newsPageSizeKey = "newsPerPage";
        private Int32 languageID;
        private const String cookieName = "_news";

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            GetCurrentLanguage();
            base.OnActionExecuting(filterContext);
        }

        private readonly INewsService _newsService;

        public NewsController(INewsService neewsService)
        {
            _newsService = neewsService;
          
        }


      //  [OutputCache(Duration = 20)]
        private void GetNewsSettings()
        {
            PageSize = _unitOfWorkAsync.RepositoryAsync<Setting>()
                                             .Query(x => x.Key == newsPageSizeKey)
                                             .Select()
                                             .FirstOrDefault()
                                             .Value;

        }

        [OutputCache(Duration = 20)]
        private void GetCurrentLanguage()
        {
            var lanquage = _lanquageService.GetCurrentLanguage(CurrentLang);

            languageID = lanquage.ID;
        }

        [ChildActionOnly]
        public ActionResult Archieve()
        {
            var model = _newsService.GetNewsArchieveLinks();


            return PartialView(model);
        }



        public ActionResult All(String category, String dt, Int32 page = 1)
        {
            GetNewsSettings();

            ViewBag.SelectedCategory = category;


            IQueryable<NewsViewModel> model=null;

            if (!String.IsNullOrEmpty(dt))
            {
                DateTime date;

                DateTime.TryParse(dt, out date);

                if (date != null)
                {
                    long startDate = date.ToUnixTime();

                    long finishDate = date.AddMonths(1).AddMinutes(-1).ToUnixTime();


                    model = _newsService.GetAllByDateTime(startDate, finishDate, langID);

                   
                }



            }
            else
            {
                model = _newsService.GetAll(languageID);


                if (!String.IsNullOrEmpty(category))
                {
                    Int32? categoryID = GetcategoryID(category);

                    if (categoryID != null)
                    {
                        model = model.Where(x => x.News.CategoryID == categoryID);
                    }
                }
            }
 

            return View(model.ToPagedList(page, PageSize));

        }


        private Int32? GetcategoryID(String title)
        {
            var category = _unitOfWorkAsync.RepositoryAsync<NewsCategory>().Query(x => x.Name == title).Select(s => s.Id).FirstOrDefault();

            return category;
        }


        public async Task<ActionResult> Show(Int32? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = _newsService.FindNewsByID(id.Value, languageID);


            if (model == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var isView = CookieHelper.IsUserView(id.Value, cookieName);

            if (!isView)
            {
                model.NewsLang.View += 1;


                var news = await _newsService.FindAsync(id);

                //   news.NewsLangs = model.View;

                news.ObjectState = ObjectState.Modified;



                _newsService.Update(news);
                await _unitOfWorkAsync.SaveChangesAsync();


            }


            return View(model);

        }
        public ActionResult RSS()
        {
            var items = new List<SyndicationItem>();

            //Витягування з бази даних топ-10 новин з сайту
            var data = _newsService.GetAll(languageID).Take(10);//OrderBy DateTime todo



            foreach (var news in data)
            {
                var item = new SyndicationItem()
                {
                    Id = news.News.Id.ToString(),
                    Title = SyndicationContent.CreatePlaintextContent(news.NewsLang.Name),
                    Content = SyndicationContent.CreateHtmlContent(news.NewsLang.ShortText),
                    PublishDate = news.News.TimeCreated.ToDateTime()
                };
                item.Links.Add(SyndicationLink.CreateAlternateLink(new Uri(Url.Action("RSS", "News", new { id = news.News.Id }))));//Nothing alternate about it. It is the MAIN link for the item.
                items.Add(item);
            }

            return new RssFeed(title: "EasyTest",
                      items: items,
                      contentType: "application/rss+xml",
                      description: "Здавайте тести легко в системі дистанційного навчанння з допомогою програми EasyTest");
        }
    }
}