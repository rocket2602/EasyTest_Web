﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace WebApp.Helpers
{

    public abstract class MathCaptcha
    {
        protected StringBuilder str;
        protected Random r;
        protected Int32[] array;

        public MathCaptcha()
        {
            str = new StringBuilder();
            r = new Random();
            array = new Int32[3];//x,y,z
        }

        private Int32 result;
        private String text;

        public String GetCaptcha(Int32 max,out Int32 res)
        {
            GenerateCaptcha(max);
            res = result;
            return text;
        }
        protected abstract void GenerateCaptcha(Int32 max);

        protected abstract String Operation { get; }
        protected abstract String OperationName { get; }

        protected void SetResult()
        {
            Int32 pos = r.Next(array.Length);

            String oper = (r.Next() % 2 == 0) ? Operation : OperationName;

            result = array[pos];

            switch (pos)
            {
                case 0: str.Append("? " + oper + " " + array[1] + " = " + array[2]); break;
                case 1: str.Append(array[0] + " " + oper + " ? = " + array[2]); break;
                case 2: str.Append(array[0] + " " + oper + array[1] + "  = ?"); break;
                default: throw new Exception("Error"); 
            }

            text = str.ToString();
        }
    }


    public class Plus : MathCaptcha
    {

        protected override String Operation
        {
            get { return "+"; }
        }

        protected override String OperationName
        {
            get { return "[Плюс]"; }
        }

        protected override void GenerateCaptcha(Int32 max)
        {
            Int32 temp;

            array[0] = r.Next(-max, max);//x

            temp = Math.Abs(array[0] + max);

            array[1] = r.Next(-temp, temp);//y

            array[2] = array[0] + array[1];//z

             SetResult();
        }
    }


    public class Minus : MathCaptcha
    {
        protected override String Operation
        {
            get { return "-"; }
        }

        protected override String OperationName
        {
            get { return "[Мінус]"; }
        }

        protected override void GenerateCaptcha(Int32 max)
        {
            Int32 temp;

            array[0] = r.Next(-max, max);//x

            temp = Math.Abs(array[0] + max);

            array[1] = r.Next(-temp, temp);//y

            array[2] = array[0] - array[1];//z

            SetResult();
        }
    }

    public  class Multiply : MathCaptcha
    {

        protected override String Operation
        {
            get { return "*"; }
        }

        protected override String OperationName
        {
            get { return "[помножити]"; }
        }
        protected override void GenerateCaptcha(Int32 max)
        {
            Int32 temp;

            array[0] = RandomNumber(max);

            temp = Math.Abs(max / array[0]);

            array[1] = RandomNumber(temp);
                   
            array[2] = array[0] * array[1];

            SetResult();
        }

        private Int32 RandomNumber(Int32 max)
        { 
            Int32 number;
             do
            {
               number = r.Next(-max, max);
            } while (number == 0);

             return number;
        }



    }


    public class Divide : MathCaptcha
    {


        protected override string Operation { get { return "/"; } }

        protected override string OperationName { get { return "[поділити]"; } }

        protected override void GenerateCaptcha(Int32 max)
        {
          
            var temp = new List<Int32>();

            array[0] = Proste(max);//x

            Int32 i = array[0] * -1 + 1;

            for (; i < array[0]; i++)//Пошук дільників числа x
            {
                if (i == 0 || i == -1 || i == 1) continue;
                if (array[0] % i == 0) temp.Add(i);
            }

            array[1] = temp[r.Next(temp.Count)];
            array[2] = array[0] / array[1];

            SetResult();
        }
        private Int32 Proste(Int32 max)
        {
            var mass = new Int32[max];

            for (Int32 i = 2; i < max; i++)
            {
                mass[i] = 0;
            }

            var sqrt = (Int32)Math.Sqrt(max) + 1;

            for (Int32 p = 2; p < sqrt; p++)
            {
                if (mass[p] == 0)
                {
                    for (Int32 i = p * p; i < max; i += p)
                    {
                        mass[i] = i;
                    }
                }
            }
            var res = mass.Where(x => x > 0).ToArray();
            return res[r.Next(res.Count())];
        }

    }


    public static class CaptchaHelper
    {

      private static Random r = new Random();


        public static String GetCaptcha(out Int32 res, Int32 max = 50)
        {
            var obj = new List<MathCaptcha>() {new  Plus(),new Minus(),new Multiply(),new Divide() };

          

            Int32 i = r.Next(obj.Count);

            return  obj[i].GetCaptcha(max,out res);
        }


      
        public static Boolean CaptchaIsValid(String answer)
        {
            return String.Compare(answer, System.Web.HttpContext.Current.Session["Captcha"].ToString(), StringComparison.Ordinal) == 0;

        }


 
        
        public static Bitmap GenerateImage(Int32 Width, Int32 Height, String Phrase)
        {
            var CaptchaImg = new Bitmap(Width, Height);
            var Randomizer = new Random();
            var Graphic = Graphics.FromImage(CaptchaImg);
            Graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Graphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            //Set height and width of captcha image
            var rect = new Rectangle(0, 0, Width, Height);
            // Fill in the background.
            var hatchBrush = new HatchBrush(
              HatchStyle.DiagonalBrick,
              Color.LightGray,
              Color.White);
            Graphic.FillRectangle(hatchBrush, rect);

            //Rotate text a little bit
            var rand = new Random((Int32)DateTime.Now.Ticks);
            int i, r, x, y;
            var pen = new Pen(Color.Yellow);
            for (i = 1; i < 10; i++)
            {
                pen.Color = Color.FromArgb(
                (rand.Next(0, 255)),
                (rand.Next(0, 255)),
                (rand.Next(0, 255)));

                r = rand.Next(0, (180 / 3));
                x = rand.Next(0, 180);
                y = rand.Next(0, 30);

                Graphic.DrawEllipse(pen, x - r, y - r, r, r);
            }
            Graphic.RotateTransform(-3);
            // Set up the text font.
            Font font;
            font = new Font(
              "Times New Roman",
              14,
              FontStyle.Bold);
            Graphic.DrawString(Phrase, font,
               new SolidBrush(Color.DimGray), 5, 5);
            Graphic.Flush();
            return CaptchaImg;
        }

        public static List<Int32> HelpMe(Int32 value,Int32 max = 50)
        {          
            var helpList = new List<Int32>();
            Int32 maxCount = 5;

            for (Int32 i = 0; i < maxCount; i++)
            {
                Int32 number = r.Next(-max, max);
                if (number == value)
                {
                    i--;
                    continue;
                }
                helpList.Add(number);
            }

            Int32 pos = r.Next(maxCount);

            helpList[pos] = value;//Підстановка справжнього значення

            return helpList;
        }
    }
}