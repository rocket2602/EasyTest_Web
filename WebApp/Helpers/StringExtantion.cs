﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Helpers
{
    public static class StringExtantion
    {

        public static String UppercaseFirst(this String s)
        { 
        
	        if (String.IsNullOrEmpty(s))
	        {
	            return String.Empty;
	        }
	        Char[] a = s.ToCharArray();
	        a[0] = Char.ToUpper(a[0]);
	        return new String(a);
        }
    }
}