﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WevApp.Helpers
{
    public static class CookieHelper
    {

        public static String GetCookie(String cookieName)
        {
            String cookie = String.Empty;
            cookie = HttpContext.Current.Request.Cookies[cookieName].Value;
            return cookie;
        }

        public static void SetCookie(String cookieName, String value, DateTime? expires = null)
        {
            var cookie = new HttpCookie(cookieName);
            cookie.Value = value;
            cookie.Expires = expires ?? DateTime.Now.AddYears(1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static bool CookieExists(string cookieName)
        {        
            var cookie = HttpContext.Current.Request.Cookies[cookieName];        
            return (cookie != null)?true:false;
        }

        private static Boolean IdExists(Int32 id,String cookieName)
        {
            String values = GetCookie(cookieName);
            var list = new List<String>();
            list.AddRange(values.Split(','));
            Int32 i =  list.BinarySearch(id.ToString());

            if (i < 0)
            {
                list.Add(id.ToString());
                values += ","+id;
                SetCookie(cookieName, values);
            }                    
            return i >= 0;
        }


        public static Boolean IsUserView(Int32 id, String cookieName)
        {
            if (CookieExists(cookieName))
            {
                return  CookieHelper.IdExists(id, cookieName);              
            }
            else
            {
                SetCookie(cookieName, id.ToString());
                return false;
            }    
        }
    }
}