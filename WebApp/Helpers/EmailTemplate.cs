﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Helpers
{
    public static class EmailTemplate
    {
        public static String ConfirmEmail(String callbackUrl)
        {
            var html= "Для підтвердження реєситрації на сайті перейдіть по  <a href=\"" + callbackUrl + "\">даному</a> посиланню";
            return html;
        }

        public static String ResetPassword(String callbackUrl)
        {
            var html = "Для Скидання пароля перейдіть по  <a href=\"" + callbackUrl + "\">даному</a> посиланню";
            return html;
        }




        public static String Register(String name, String email, String password)
        {
            var htmlString = @"<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset='utf-8' />
    <style>
        body {
            background: #808080;
            font-family:'Comic Sans MS',sans-serif;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            border-radius: 6px;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            background: #ffffff;
        }
        .logo {    
            margin-left: 200px;
        }
        .message {
            font-size:14px;
        }
    </style>
</head>
<body>
    <div class='container'>
        <table>
            <tr>
                <td><img class='logo' src='http://easytest.com.ua/Content/Images/emaillogo.png' width='200' height='50' alt='EasyTest' /></td>
            </tr>
            <tr>
                <td style='padding:0 15px;'>
                    <h3>Реєстрація в програмі</h3>
                    <div class='message'>
                        <p>

                            Вітаю,<span style='color:#0026ff'> "
                + name +
                @"</span>
                        </p>
                        <p>Для входу скористайтеся наступними даними:</p>
             
           <p><span style='font-weight:bold;'>Ваш email:</span> "
                + email +
            @"</p>
                        <p><span style='font-weight:bold;'>Ваш пароль:</span> 
" +
 password
 +
@"</p>
                        <p>Приємного користуання </p>

                        <p style='color:#ff0000;'>Якщо ви не реєструвалися в програмі, проігноруйте дане повідомлення</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='margin-left:15px;'>
                        <tr>

                            <td>
                                <img src='http://easytest.com.ua/Content/Images/d1813a773c115772_48x48.png' alt='Icon'>
                            </td>
                            <td style='font-family:'Bookman Old Style',sans-serif;vertical-align:top'>

                                <span style='font-weight:bold;'>
                                    EasyTest!<br>
                                </span><span style='line-height:2'>Здавайте тести легко та на найвищу оцінку</span>
                            </td>       
                        </tr>

                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>";
            return htmlString;
        }

    }
}