﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace WebApp.Helpers
{
    public static class LinqExtansion
    {
        public static IQueryable<T> OrderBy<T>(
       this IQueryable<T> query, string sortColumn, string direction)
        {
            string methodName = string.Format("OrderBy{0}",
              direction.ToLower() == "asc" ? "" : "descending");
            ParameterExpression parameter = Expression.Parameter(query.ElementType, "p");
            MemberExpression memberAccess = null;
            //
            foreach (var property in sortColumn.Split('.'))
            {
                memberAccess = MemberExpression.Property(memberAccess ?? (parameter as Expression), property);
            }
            //
            LambdaExpression orderByLambda = Expression.Lambda(memberAccess, parameter);
            MethodCallExpression result = Expression.Call(
                        typeof(Queryable),
                        methodName,
                        new[] { query.ElementType, memberAccess.Type },
                        query.Expression,
                        Expression.Quote(orderByLambda));
            //
            // For the sortColumn=='User.Username' and  direction=="desc" ==> 
            //           "OrderByDescending(p => p.User.Username)" expression 
            // will be appended to the input query!
            //
            return query.Provider.CreateQuery<T>(result);
        }

    }
}