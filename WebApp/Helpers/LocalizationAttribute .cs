﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Helpers
{
    public class LocalizationAttribute : ActionFilterAttribute
    {
        private const String CookieName = "_culture";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            String cultureName = filterContext.RouteData.Values["culture"] as String;

            if (cultureName == null)
                cultureName = GetSavedCultureOrDefault(filterContext.RequestContext.HttpContext.Request);

            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe

            if (filterContext.RouteData.Values["culture"] as String != cultureName)
                // Force a valid culture in the URL
                filterContext.RouteData.Values["culture"] = cultureName.ToLowerInvariant(); // lower case too

            SetCultureOnThread(cultureName);

            SaveCultureInCookie(filterContext.HttpContext.Response, cultureName);

            base.OnActionExecuting(filterContext);
        }

        private void SaveCultureInCookie(HttpResponseBase httpResponseBase, String cultureName)
        {
            var _cookie = new HttpCookie(CookieName, cultureName);
            _cookie.Expires = DateTime.Now.AddYears(1);
            httpResponseBase.SetCookie(_cookie);
        }

        private String GetSavedCultureOrDefault(HttpRequestBase httpRequestBase)
        {
            var cookie = httpRequestBase.Cookies[CookieName];

            if (cookie != null)
            {
                return cookie.Value;
            }
            else
            {
                var HTTPHeadeAcceptLanguages = httpRequestBase.UserLanguages;
                return HTTPHeadeAcceptLanguages != null && HTTPHeadeAcceptLanguages.Length > 0 ? HTTPHeadeAcceptLanguages[0] : null; // obtain it from HTTP header AcceptLanguages

            }
        }

        private void SetCultureOnThread(String culture)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }
    }
}