﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApp.App_Start;
using WebApp.Areas.AdminAuth.Models;
using WebApp.Infrastructure;

namespace WebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {

     //   private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {           
          //  EFlogger.EntityFramework6.EFloggerFor6.Initialize();//Delete in Release
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.Add(typeof(GridSettings), new GridModelBinder());


        }
    }
}
