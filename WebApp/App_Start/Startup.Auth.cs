﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebApp.Models;
using WebApp.Providers;

namespace WebApp
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);



            var url = new UrlHelper(HttpContext.Current.Request.RequestContext);

            //Create the wrapper
            var provider = new CookieAuthenticationProvider();
            var providerWrapper = new CookieAuthenticationProviderWrapper(provider);

            // Our logic to dynamically modify the path (maybe needs some fine tuning)
            providerWrapper.OnApplyingRedirect = context =>
            {
                var mvcContext = new HttpContextWrapper(HttpContext.Current);
                var routeData = RouteTable.Routes.GetRouteData(mvcContext);

                //Get the current language  
                RouteValueDictionary routeValues = new RouteValueDictionary();
                routeValues.Add("culture", routeData.Values["culture"]);

                //Reuse the RetrunUrl
                Uri uri = new Uri(context.RedirectUri);
                string returnUrl = HttpUtility.ParseQueryString(uri.Query)[context.Options.ReturnUrlParameter];
                routeValues.Add(context.Options.ReturnUrlParameter, returnUrl);

                //Overwrite the redirection uri
                context.RedirectUri = url.Action("Login", "Account", routeValues);
            };

            app.UseCookieAuthentication(new CookieAuthenticationOptions()

            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString(url.Action("Login", "Account")),
                Provider = providerWrapper
            }
            );


            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));


            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);



            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //appId: "gthjtyky",
            //       appSecret: "tjykuii");

            //    app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //    {
            //        ClientId = "ttyiyukyu",
            //        ClientSecret = "gtrjyli"
            //    });
        }


    }
    public class CookieAuthenticationProviderWrapper : ICookieAuthenticationProvider
    {
        private CookieAuthenticationProvider _cookieAuthenticationProvider;

        public CookieAuthenticationProviderWrapper(CookieAuthenticationProvider cookieAuthenticationProvider)
        {
            _cookieAuthenticationProvider = cookieAuthenticationProvider;
            OnApplyingRedirect = context => { };
            OnRedirectApplied = context => { };
        }

        public Action<CookieApplyRedirectContext> OnRedirectApplied { get; set; }

        public Action<CookieApplyRedirectContext> OnApplyingRedirect { get; set; }

        public void ApplyRedirect(CookieApplyRedirectContext context)
        {
            //if (!IsAjaxRequest(context.Request))
            //{
            //    context.Response.Redirect(context.RedirectUri);
            //}
            OnApplyingRedirect.Invoke(context);
            _cookieAuthenticationProvider.ApplyRedirect(context);
            OnRedirectApplied.Invoke(context);
        }

        public void ResponseSignIn(CookieResponseSignInContext context)
        {
            _cookieAuthenticationProvider.ResponseSignIn(context);
        }

        public System.Threading.Tasks.Task ValidateIdentity(CookieValidateIdentityContext context)
        {
            return _cookieAuthenticationProvider.ValidateIdentity(context);
        }

        public void Exception(CookieExceptionContext context)
        {
            _cookieAuthenticationProvider.Exception(context);
        }

        public void ResponseSignOut(CookieResponseSignOutContext context)
        {
            _cookieAuthenticationProvider.ResponseSignOut(context);

        }

        public void ResponseSignedIn(CookieResponseSignedInContext context)
        {
            _cookieAuthenticationProvider.ResponseSignedIn(context);
        }

        private static bool IsAjaxRequest(IOwinRequest request)
        {
            IReadableStringCollection query = request.Query;
            if ((query != null) && (query["X-Requested-With"] == "XMLHttpRequest"))
            {
                return true;
            }
            IHeaderDictionary headers = request.Headers;
            return ((headers != null) && (headers["X-Requested-With"] == "XMLHttpRequest"));
        }
    }
}