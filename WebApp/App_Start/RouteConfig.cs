﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebApp.App_Start;
using WebApp.Helpers;

namespace WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            var dataTokens = new RouteValueDictionary();

            var ns = new String[] { "WebApp.Controllers" };

            dataTokens["Namespaces"] = ns;



            routes.Add(
                  "NewsDetail",
                 new SeoFriendlyRoute(
                     "News/Show/{id}",
                     new RouteValueDictionary(new { culture = CultureHelper.GetDefaultCulture(), controller = "News", action = "Show",id=UrlParameter.Optional }),
                     null,
                     dataTokens)
                );

            routes.MapRoute(
                name: "Default",
                url: "{culture}/{controller}/{action}/{id}",
                defaults: new { culture = CultureHelper.GetDefaultCulture(), controller = "Home", action = "Index", id = UrlParameter.Optional },
                constraints: new { culture = @"\w{2,3}(-\w{4})?(-\w{2,3})?" },
                namespaces: new[] { "WebApp.Controllers" }

            );
        }
    }
}
