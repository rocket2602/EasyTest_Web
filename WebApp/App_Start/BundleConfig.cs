﻿using System.Web;
using System.Web.Optimization;

namespace WebApp
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                          "~/Scripts/jquery.unobtrusive-ajax*",
                          "~/Scripts/jquery.validate*"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/bootstrap3/js/bootstrap.min.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap-dialog.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap3/bootstrap.min.css",
                      "~/Content/css/bootstrap/bootstrap-theme.css",
                      "~/Content/css/Style.css",
                      "~/Content/css/msdropdown/dd.css",
                      "~/Content/css/msdropdown/flags.css",
                      "~/Content/css/font-awesome.css",
                       "~/Content/css/bootstrap-dialog.min.css"));

			bundles.Add(new StyleBundle("~/bundles/login").Include(
					 "~/Content/css/bootstrap/bootstrap.css",
					 "~/Content/css/admin/AdminLTE.css",
					 "~/Plugins/iCheck/square/blue.css",			 
                     "~/Content/css/font-awesome.css"));


			bundles.Add(new StyleBundle("~/Content/admin").Include(
                     "~/Content/css/bootstrap/bootstrap.css",
                     "~/Content/css/bootstrap/bootstrap-theme.css",
                     "~/Content/css/adminStyle.css",
                      "~/Content/css/font-awesome.css",
                       "~/Content/css/msdropdown/dd.css",
                      "~/Content/css/msdropdown/flags.css",
                      "~/Content/css/bootstrap-dialog.min.css"
                   ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
