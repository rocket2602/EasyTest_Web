﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApp.App_Start
{
    public class SeoFriendlyRoute : Route
    {
        public SeoFriendlyRoute(String url, RouteValueDictionary defaults, RouteValueDictionary constraints, RouteValueDictionary dataTokens)
            : base(url, defaults, constraints, dataTokens, new MvcRouteHandler())
        {

        }

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            var routeDate = base.GetRouteData(httpContext);

            if (routeDate != null)
            {
                if (routeDate.Values.ContainsKey("id"))
                {
                    routeDate.Values["id"] = GetIdValue(routeDate.Values["id"]);
                }
            }

            return routeDate;
        }

        private object GetIdValue(object id)
        {
            if (id != null)
            {
                String idValue = id.ToString();

                var regex = new Regex(@"^(?<id>\d+).*$");

                var match = regex.Match(idValue);

                if (match.Success)
                {
                    return match.Groups["id"].Value;
                }
            }
            return id;
        }
    }
}