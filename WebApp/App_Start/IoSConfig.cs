﻿using EasyTest.Entity;
using EasyTest.Service;
using EasyTestData;
using Ninject;
using Repository.Pattern.DataContext;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebApp.App_Start
{
    public class IoSConfig
    {
        public class NinjectDependencyResolver : IDependencyResolver
        {
            private IKernel kernel;
            public NinjectDependencyResolver(IKernel kernelParam)
            {
                kernel = kernelParam;
                AddBindings();
            }
            public object GetService(Type serviceType)
            {
                return kernel.TryGet(serviceType);
            }
            public IEnumerable<object> GetServices(Type serviceType)
            {
                return kernel.GetAll(serviceType);
            }
            private void AddBindings()
            {
                kernel.Bind<IDataContextAsync>().To<EasyTestContext>().InSingletonScope();
                kernel.Bind<IUnitOfWorkAsync>().To<UnitOfWork>().InSingletonScope();




                #region Service
                kernel.Bind<INewsService>().To<NewsService>();
                kernel.Bind<IRepositoryAsync<News>>().To<RepositoryAsync<News>>();
                kernel.Bind<IRepositoryAsync<NewsLang>>().To<RepositoryAsync<NewsLang>>();

                kernel.Bind<INewsCategoryService>().To<NewsCategoryService>();
                kernel.Bind<IRepositoryAsync<NewsCategory>>().To<RepositoryAsync<NewsCategory>>();

                kernel.Bind<IQuizService>().To<QuizService>();
                kernel.Bind<IRepositoryAsync<Quiz>>().To<RepositoryAsync<Quiz>>();


                kernel.Bind<IQuizQuestionService>().To<QuizQuestionService>();
                kernel.Bind<IRepositoryAsync<QuizQuestion>>().To<RepositoryAsync<QuizQuestion>>();


                kernel.Bind<IUniversityService>().To<UniversityService>();
                kernel.Bind<IRepositoryAsync<University>>().To<RepositoryAsync<University>>();

                kernel.Bind<ICountryService>().To<CountryService>();
                kernel.Bind<IRepositoryAsync<Country>>().To<RepositoryAsync<Country>>();

                kernel.Bind<ILanguageService>().To<LanguageService>();
                kernel.Bind<IRepositoryAsync<Language>>().To<RepositoryAsync<Language>>();

                kernel.Bind<IAppService>().To<AppService>();
                kernel.Bind<IRepositoryAsync<App>>().To<RepositoryAsync<App>>();
                kernel.Bind<IRepositoryAsync<AppChanges>>().To<RepositoryAsync<AppChanges>>();


                kernel.Bind<IDepartamentService>().To<DepartamentService>();
                kernel.Bind<IRepositoryAsync<Departament>>().To<RepositoryAsync<Departament>>();


                kernel.Bind<ICathedraService>().To<CathedraService>();
                kernel.Bind<IRepositoryAsync<Cathedra>>().To<RepositoryAsync<Cathedra>>();

                kernel.Bind<IPageService>().To<PageService>();
                kernel.Bind<IRepositoryAsync<Page>>().To<RepositoryAsync<Page>>();

                kernel.Bind<IUserService>().To<UserService>();
                kernel.Bind<IRepositoryAsync<User>>().To<RepositoryAsync<User>>();

                kernel.Bind<ILogInService>().To<LogInService>();
                kernel.Bind<IRepositoryAsync<LogInSingleton>>().To<RepositoryAsync<LogInSingleton>>();


                #endregion

            }
        }
    }
}