﻿using EasyTest.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace WebApp.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
       
        public string ReturnUrl { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Display(Name = "LastName", ResourceType = typeof(Resource.GlobalRes))]
        [Required]
        public String LastName { get; set; }

        [Display(Name = "FirstName", ResourceType = typeof(Resource.GlobalRes))]
        [Required]
        public String FirstName { get; set; }

        [Display(Name = "MiidleName", ResourceType = typeof(Resource.GlobalRes))]
        public String MiddleName { get; set; }



		[Required]
        [EmailAddress]
        [Remote("ValidateEmail", "Account", HttpMethod = "Post", ErrorMessage = "[Email is not available.]")]
        [Display(Name = "Email", ResourceType = typeof(Resource.GlobalRes))]
		public string Email { get; set; }


		[Display(Name = "Password", ResourceType = typeof(Resource.GlobalRes))]
		[Required]
        [MinLength(8)]
		public String Password { get; set; }

        [Required]
        public Int32 CountryID { get; set; }
	    [Display(Name="[Країна]")]
		public IEnumerable<Country> Countries { get; set; }
        [Required, Display(Name = "University", ResourceType = typeof(Resource.GlobalRes))]
        public int UniversityID { get; set; }
        [Required, Display(Name = "Departament", ResourceType = typeof(Resource.GlobalRes))]
        public int DepartamentID { get; set; }
        [Required, Display(Name = "Cathedra", ResourceType = typeof(Resource.GlobalRes))]
        public int CathedraID { get; set; }
        [Required,Range(1,15)]

        [Display(Name="Course",ResourceType=typeof(Resource.GlobalRes))]
        
        public Byte Course { get; set; }

        [Display(Name = "Group", ResourceType = typeof(Resource.GlobalRes))]
        public String Group { get; set; }


        [Required(ErrorMessage="Поставте галочку")]
        public Boolean Terms { get; set; }

       
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }
    }


    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
}
