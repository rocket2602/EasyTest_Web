﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class CaptchaModel
    {
        [Required(ErrorMessage = "[Введіть відповідь]")]
        public String CaptchaAnswer { get; set; }
    }
}