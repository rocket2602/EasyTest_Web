﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Text;
using System.Security.Cryptography;
using System.ComponentModel.DataAnnotations.Schema;
using Model.BL;
using EasyTest.Entity;

namespace WebApp.Models
{
    public class MyUser : IdentityUser<int, MyLogin, MyUserRole, MyClaim>
    {
      //  public virtual UsersInfo UserInfo { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<MyUser, int> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
    public class MyUserRole : IdentityUserRole<int> { }

    public class MyRole : IdentityRole<int, MyUserRole>
    {
        public MyRole() : base() { }
        public MyRole(String name, String description)
            : base()
        {
            this.Description = description;
        }
        public virtual String Description { get; set; }
    }
    public class MyClaim : IdentityUserClaim<int> { }
    public class MyLogin : IdentityUserLogin<int> { }

   

    public class CustomUserStore : UserStore<MyUser, MyRole, int, MyLogin, MyUserRole, MyClaim>
    {
        public CustomUserStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }

    public class CustomRoleStore : RoleStore<MyRole, int, MyUserRole>
    {
        public CustomRoleStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }

    public class MyPasswordHasher : PasswordHasher
    {
        public override PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            return hashedPassword.Equals(HashPassword(providedPassword)) ? PasswordVerificationResult.Success : PasswordVerificationResult.Failed;
        }
        public override string HashPassword(string password)
        {
            return   HashHelper.SHA1HashStringForUTF8String(password);
        }

        

    }



    public class ApplicationDbContext : IdentityDbContext<MyUser, MyRole, int, MyLogin, MyUserRole, MyClaim>
    {
        public ApplicationDbContext()
            : base("MyIdentityConnection")
        {

        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<MyUser>().ToTable("Users");
            modelBuilder.Entity<MyUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<MyRole>().ToTable("Roles");
            modelBuilder.Entity<MyClaim>().ToTable("UserClaims");
            modelBuilder.Entity<MyLogin>().ToTable("UserLogins");
            modelBuilder.Entity<MyUser>().Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<MyRole>().Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<MyClaim>().Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<MyUser>().Property(r => r.PasswordHash).HasColumnName("Password");

        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
    public class IdentityManager
    {
        RoleManager<MyRole, int> _roleManager = new RoleManager<MyRole, int>(
       new CustomRoleStore(new ApplicationDbContext()));

        UserManager<MyUser, int> _userManager = new UserManager<MyUser, int>(
            new CustomUserStore(new ApplicationDbContext()));

        ApplicationDbContext _db = new ApplicationDbContext();
        public bool RoleExists(string name)
        {

            return _roleManager.RoleExists(name);
        }


        public bool CreateRole(string name, string description = "")
        {
            // Swap ApplicationRole for IdentityRole:
            var idResult = _roleManager.Create(new MyRole(name, description));
            return idResult.Succeeded;
        }


        public bool CreateUser(MyUser user, string password)
        {
            var idResult = _userManager.Create(user, password);
            return idResult.Succeeded;
        }


        public bool AddUserToRole(string userId, string roleName)
        {
            var um = new UserManager<MyUser, int>(
                new CustomUserStore(new ApplicationDbContext()));
            var idResult = um.AddToRole(Int32.Parse(userId), roleName);
            return idResult.Succeeded;
        }

    }
}