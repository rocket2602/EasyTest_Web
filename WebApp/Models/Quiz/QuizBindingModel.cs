﻿using EasyTest.BL.Quiz.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Quiz
{
    public class QuizBindingModel<T> where T : QuizQuestionModel
    {
        [Required]
        public Int32 tid { get; set; }

        [Required]
        [StringLength(255, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public String QuizName { get; set; }

        [Required]
        public List<T> Questions { get; set; }
    }
}