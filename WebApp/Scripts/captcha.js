﻿(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node / CommonJS
        factory(require('jquery'));
    } else {
        factory(jQuery);
    }
})(function ($) {

    'use strict';

    var console = window.console || { log: function () { } };

    function MatchCaptcha($element) {
        this.$container = $element;
       
        this.$image = this.$container.find("#captchaImage");
        this.$refreshBtn = this.$container.find('#refreshCaptcha');
        this.$helpBtn = this.$container.find("#captchaHelp");      
        this.$helpList = this.$container.find('#captchaAnswers');
        this.$result = this.$container.find('#CaptchaAnswer');
        this.$loading = this.$container.find('.loading');
        this.init();
    }
    MatchCaptcha.prototype = {
        constructor: MatchCaptcha,
        init: function () {
          
            this.$refreshBtn.on('click', $.proxy(this.refresh, this));
            this.$helpBtn.on('click', $.proxy(this.getHelp, this));       
        },
        refresh: function (e) {     
            e.preventDefault();
          
             var src = this.$image.attr("src");    
             this.$image.attr("src", src);
             this.$helpList.html("");         
             this.$result.val('');

             this.$helpBtn.show();
            
        },
        getHelp: function (e) {
            e.preventDefault();
            this.ajaxImageUpdate();          
        },
        setResult: function () {
            
            var res = $(this).val();
          $('#CaptchaAnswer').val(res);
        },
        ajaxImageUpdate: function () {
            var url = this.$helpBtn.attr('href'),
                _this = this;
                      
            $.ajax(url, {
                type: 'post',       
                dataType: 'json',
                processData: false,
                contentType: false,

                beforeSend: function () {
                    _this.submitStart();
                },

                success: function (data) {
                    _this.submitDone(data);
                },

                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    _this.submitFail(textStatus || errorThrown);
                },

                complete: function () {
                    _this.submitEnd();
                }
            });
        },
        submitStart: function () {
            this.$loading.show();
            this.$helpBtn.hide();
        },
        submitDone: function (data) {
                var list = $(this.$helpList);
          
                $.each(data, function (index, value) {         
                    var input = "<label style='float-left;margin-right:15px;' ><input type='radio' name='help' class='helpMe' value='" + value + "' > " + value + "</label>";                        
                    list.append(input);
                });             
        },
        submitFail: function (msg) {
            this.alert(msg);
        },
        submitEnd: function () {        
            this.$loading.hide();     
            this.$container.delegate('.helpMe', 'click', this.setResult);
            
        },
        alert: function (msg) {
            var $alert = [
                  '<div class="alert alert-danger avater-alert">',
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>',
                    msg,
                  '</div>'
            ].join('');
            this.$container.before($alert);
        }
    };

    $(function () {
        return new MatchCaptcha($('#CaptchaContainer'));
    });
});

