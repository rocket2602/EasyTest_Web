﻿    function GetTranslatedLanguages(cellval, options, rowObject) {

        var array = rowObject['Translated'].toString().split(',');

        var languages = $('#' + options.gid).jqGrid('getGridParam', 'userData');

        var rowObjectId;

        if(!options.colModel.formatoptions.entity)       
            rowObjectId = rowObject[options.colModel.formatoptions.property]
        else
            rowObjectId = rowObject["Entity"][options.colModel.formatoptions.property];


        console.log(rowObjectId);
        
        var output = ' <select   form="form' + options.gid + rowObjectId + '"    role="select"  class="' +
                        options.colModel.formatoptions.select_style +
                        ' editable" name="' +
                        options.colModel.formatoptions.name +
                        '" size="1" style="width:140px">';

        var optionHtml = '';

        for (var i = 0; i < languages.length; i++) {

            var exist = $.inArray(languages[i]["Id"].toString(), array) != -1;

            if (exist && options.colModel.formatoptions.containse)
                optionHtml += SetOptions(languages[i]);

            if (!exist && !options.colModel.formatoptions.containse)
                optionHtml += SetOptions(languages[i]);
        }

        (optionHtml.length != 0) ? output += optionHtml + '</select>' : output = '';

        return output;
    }

    function SetOptions(lang) {

        var result = '';

        result += '<option value= "' + lang['Id'] + '"';
        result += 'data-image="/Content/Images/blank.gif"';
        result += 'data-imagecss="flag ' + lang["Icon"] + '"';
        result += 'data-title="' + lang["NativeName"] + '">';
        result += lang["NativeName"]
        result += '</option>';

        return result;
    }

    function SetForm(cellval, options, rowObject) {
      
        return '<form id="form' +options.gid+ rowObject[options.colModel.formatoptions.elem] + '" method="post" action="' + options.colModel.formatoptions.url + '/' + rowObject[options.colModel.formatoptions.elem] + '"></form>';
    };

    function SetEntityForm(cellval, options, rowObject) {
        var value = rowObject[options.colModel.formatoptions.entityName][options.colModel.formatoptions.property];
        return '<form id="form' + options.gid + value + '" method="post" action="' + options.colModel.formatoptions.url + '/' + value + '"></form>';
    };

    function OriginalNameFormat(cellval, options, rowObject) {

        if (rowObject["Status"]) {
            return cellval;
        }

        return "<span style='color:#f00'>" + cellval + "</span>";
    };

    function GridRowIsNotSelected(name,selr) {
        if (!selr) {

            BootstrapDialog.show({
                setSize: BootstrapDialog.SIZE_SMALL,
                type: BootstrapDialog.TYPE_WARNING,
                title: '[Попередження]',
                message: '[Оберіть запис для виконання даної оберації]',
            });

          
            return true;        
        }

        return false;
    };

    function GridRowTranslated(name,selr) {
    
      //  var CultureFrom = $(name+' tr#' + selr).find("select[name='CultureFrom']").val();

        var CultureTo = $(name+' tr#' + selr).find("select[name='CultureTo']").val();

        if (CultureTo == undefined) {

            BootstrapDialog.show({
                size:BootstrapDialog.SIZE_SMALL,
                type:  BootstrapDialog.TYPE_WARNING,
                title: '[Попередження]:',
                message: '[Запис не потребує перекладу]',            
            });
            return true;
        }
        return false;
    };
   
    function DeleteRecord(url, gridName,data) {

        var dialog = BootstrapDialog.show({
            size: BootstrapDialog.SIZE_SMALL,
            title: '[Попередження]:',
            message: '[Ви справді хочете видалити запис]',
            buttons: [{
                label: '[Видалити]',
                cssClass: 'btn-danger',
                action: function () {

                    $.post(url,data, function (result) {

                        if (result) {                          
                            $("#"+gridName).jqGrid({ datatype: 'json', page: 1 }).trigger("reloadGrid");
                            dialog.close();                                                
                        }
                        else {

                            console.log("false " + result);
                        }

                    });
                }
            }]
        });
    };
    
    function getURLParameter(name) {

        return decodeURIComponent(
            (RegExp('[?|&]' + name + '=' + '(.+?)(&|$)').exec(location.search) || [null, null])[1]
        );
    }

    function setURLParameter(key, value) {

        if (typeof (history.pushState) != "undefined") {
            history.pushState(null, null, "?" + key + "=" + value);
        } else {
            alert("Browser does not support HTML5.");
        }
    }

