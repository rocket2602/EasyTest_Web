﻿$(function () {

    var gridName = '#gridUniversityHierarchy';

    $(gridName).jqGrid({/*University*/
        mtype: "POST",
        datatype: "json",
        caption: '[Університети[Фак.][Каф.]]',
        colModel: [
            { label: 'Id', name: 'Id', key: true, hidden: true },
            { label: paramFromView.Name, name: 'UniversitiesLang.Name', width: 150 },
            { label: paramFromView.CountryName, name: 'CountryId', width: 150 },
            { label: paramFromView.City, name: 'UniversitiesLang.City', width: 150 },
            { label: paramFromView.LMS, name: 'LMSId', width: 150 },
            { label: 'Domain', name: 'Domain', width: 200 },
            { label: paramFromView.DateTime, name: 'DateTime', formatter: 'date', formatoptions: { srcformat: 'Y-m-d H:i:s', newformat: 'd F Y' } },
            { label: 'Опубліковано', name: 'Status', formatter: 'checkbox', align: 'center', width: 40 }
        ],
        loadonce: true,
        width: 1000,
        height: 400,
        rowNum: 10,
        rowList: [10, 20, 50],
        sortname: 'UniversitiesLang.Name',
        sortorder: 'asc',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false
        },
        subGrid: true,
        subGridRowExpanded: showDepartamentHierarchyGrid,
        pager: "#gridUniversityHierarchyPager"
    });

    var universityID,departamentGridId;

    function showDepartamentHierarchyGrid(parentRowID, parentRowKey) {/*Departaments*/

        var childGridID = parentRowID + "_table";

        var childGridPagerID = parentRowID + "_pager";
   
        var childGridURL = paramFromView.DepartamentHierarchyGridUrl + '/' + parentRowKey;

        universityID = parentRowKey;

        departamentGridId = childGridID;
  
        $('#' + parentRowID).append('<table id=' + childGridID + '></table><div id=' + childGridPagerID + ' class=scroll></div>');

        $("#" + childGridID).jqGrid({
            url: childGridURL,
            mtype: "POST",
            datatype: "json",
            caption: '[Факультети]',
            colModel: [
                { label: 'Id', name: 'Entity.Id', hidden: true, key: true },
                { label: 'ParentId', name: 'Entity.ParentId', hidden: true },
                { label: paramFromView.Name, name: 'Entity.Name', formatter: OriginalNameFormat },
            ],
            //   loadonce: true,
            width: 970,
            height: '100%',
            rowNum: 10,
            rowList: [10, 20, 50],
            sortname: 'Entity.Name',
            sortorder: 'asc',
            jsonReader: {
                root: "rows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems: false
            },
            subGrid: true,
            subGridRowExpanded: showThirdCathedraHierarchyGrid,
            pager: "#" + childGridPagerID
        });

        $("#" + childGridID).jqGrid('navGrid', "#" + childGridPagerID, {/*Departaments menu*/
            edit: false, add: false, del: false, search: false,
            refresh: false,
        }).navButtonAdd("#" + childGridPagerID, {/*Delete*/
                caption: "",
                title: "Вид.",
                buttonicon: "fa fa-trash-o",
                onClickButton: function () {

                    var selr = jQuery("#" + childGridID).jqGrid('getGridParam', 'selrow');

                    var myRowData = $("#" + departamentGridId).jqGrid("getRowData", selr);               

                    if (GridRowIsNotSelected(childGridID, selr)) return;

                    var url = paramFromView.DeleteDepartamentInHierarchyUrl;

                    var data = { UniversityId: parentRowKey, DepartamentParentId: myRowData["Entity.ParentId"] };

                    DeleteRecord(url, childGridID, data);
                },
                position: "first"

            }).navButtonAdd("#" + childGridPagerID,/*Add*/
          {
              caption: "",
              title: "Додати",
              buttonicon: "ui-icon-plus",
              onClickButton: function () {

                  var myRowData = $("#" + departamentGridId).jqGrid("getRowData", parentRowKey);

                  //unbind event from dynamic generated form
                  $(document.body).off("submit", "#UniversityDepartamentForm");
                
                  var dialog = BootstrapDialog.show({
                      title: '[Додати факультети]',                   
                      message: $("<div></div>").load(paramFromView.AddDepartamentInHierarchyUrl + '/' + parentRowKey)
                  });

                 $(document.body).on("submit", "#UniversityDepartamentForm", function (e) {
                     
                      e.preventDefault();
                    
                      $.post($(this).attr('action'), $(this).serialize(), function (json) {
                          if (json) {
                              $("#" + childGridID).jqGrid({ datatype: 'json', page: 1 }).trigger("reloadGrid");                           
                              dialog.close();
                          }
                          else {

                              console.log("false " + json);
                          }
                      }, 'json');
                 });           
              }
          });
    };

    var DepartamentParentId;

    function showThirdCathedraHierarchyGrid(parentRowID, parentRowKey) {/*Cathedra*/

        var childGridID = parentRowID + "_table";

        var childGridPagerID = parentRowID + "_pager";

        var myRowData = $("#" + departamentGridId).jqGrid("getRowData", parentRowKey);

        DepartamentParentId = myRowData["Entity.ParentId"];

        var childGridURL = paramFromView.CathedraHierarchyGridUrl + '/' + DepartamentParentId + "?UniversityID=" + universityID;

        $('#' + parentRowID).append('<table id=' + childGridID + '></table><div id=' + childGridPagerID + ' class=scroll></div>');

        $("#" + childGridID).jqGrid({
            url: childGridURL,
            mtype: "POST",
            datatype: "json",
            caption: '[Кафедри]',
            colModel: [
                { label: 'Id', name: 'Entity.Id', hidden: true, key: true },
                { label: 'ParentId', name: 'Entity.ParenId', hidden: true },
                { label: paramFromView.Name, name: 'Entity.Name', formatter: OriginalNameFormat },
            ],
            //   loadonce: true,
            width: 940,
            height: '100%',
            rowNum: 10,
            rowList: [10, 20, 50],
            sortname: 'Entity.Name',
            sortorder: 'asc',
            jsonReader: {
                root: "rows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems: false
            },
            pager: "#" + childGridPagerID
        });

        $("#" + childGridID).jqGrid('navGrid', "#" + childGridPagerID, {/*Cathedra menu*/
            edit: false, add: false, del: false, search: false, refresh: false,       
        }).navButtonAdd("#" + childGridPagerID, {/*Delete*/
             caption: "",
             title: "Вид.",
             buttonicon: "fa fa-trash-o",
             onClickButton: function () {

                 var selr = jQuery("#" + childGridID).jqGrid('getGridParam', 'selrow');

                 if (GridRowIsNotSelected(childGridID, selr)) return;

                 var url = paramFromView.DeleteCathedraInHierarchyUrl;

                 var data = { UniversityId: universityID, DepartamentParentId: DepartamentParentId, CathedraParentId: selr };

                 DeleteRecord(url, childGridID, data);
             },
             position: "first"

         }).navButtonAdd("#" + childGridPagerID,/*Add*/
       {
           caption: "",
           title: "Додати",
           buttonicon: "ui-icon-plus",
           onClickButton: function () {

               //unbind event from dynamic generated form
               $(document.body).off("submit", "#UniversityDepartamentCathedraForm");

               var dialog = BootstrapDialog.show({
                   title: '[Додати Кафедру]',
                   message: $("<div></div>").load(paramFromView.AddCathedraInHierarhyUrl + '/' + DepartamentParentId + "?UniversityID=" + universityID),                
               });

               $(document.body).on("submit", "#UniversityDepartamentCathedraForm", function (e) {                        
                   e.preventDefault();
                  
                   $.post($(this).attr('action'), $(this).serialize(), function (json) {
                       if (json) {
                           $("#" + childGridID).jqGrid().trigger("reloadGrid");
                           dialog.close();
                       }
                       else {
                           console.log("false " + json);
                       }
                   }, 'json');               
               });
           }
       });
    };
});