﻿$(function () {


    var gridName = "#gridUniversity",
        pager = "#pagerUniversity";


    $(gridName).jqGrid({
        mtype: 'POST',
        datatype: "json",
        caption: '[Університети]',
        colModel: [
        { label: 'Id', key: true, hidden: true, name: "Id" },
        { label: 'form', hidden: true, formatter: SetForm, formatoptions: { url: paramFromView.UniversityTranslateUrl, elem: 'Id' } },
        { label: paramFromView.Name, name: "UniversitiesLang.Name", align: 'center' },
        { label: paramFromView.CountryName, name: "CountryId", align: 'center' },
        { label: paramFromView.City, name: "UniversitiesLang.City", align: 'center' },
        { label: paramFromView.LMS, name: "LMSId", align: 'center' },
        { label: paramFromView.DateTime, name: "DateTime", align: 'center', formatter: 'date', formatoptions: { srcformat: 'Y-m-d H:i:s', newformat: 'd F Y' } },
        { label: 'Status', name: "Status", formatter: 'checkbox', align: 'center', width: 40 },
        { label: paramFromView.Translated, sortable: false, name: "Translated", formatter: GetTranslatedLanguages, formatoptions: { property: 'Id', entity:false, name: 'CultureFrom', select_style: 'LanguageFrom', containse: true }, },
        { label: paramFromView.NotTranslate, sortable: false, name: "CultureTo", formatter: GetTranslatedLanguages, formatoptions: { property: 'Id', entity: false, name: 'CultureTo', select_style: 'LanguageTo', containse: false }, }
        ],
        viewrecords: true,
        pager: $(pager),
        rowNum: 10,
        rowList: [10, 20, 50],
        width: '100%',
        height: '300',
        sortname: 'Id',
        sortorder: 'asc',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            userdata: "languages",
            repeatitems: false

        },
        loadComplete: function () {
            $('.LanguageFrom').msDropDown();
            $('.LanguageTo').msDropDown();
        },
    });

    jQuery(gridName).jqGrid('navGrid', pager, {
        edit: false, add: false, del: false, search: false, refresh: false,
    })
        .navButtonAdd(pager, {/*Del*/
            caption: "",
            title: "Вид.",

            onClickButton: function () {

                var selr = $(gridName).jqGrid('getGridParam', 'selrow');

                if (GridRowIsNotSelected(gridName, selr)) return;

                var url = paramFromView.UniversityDeleteUrl + "?id=" + selr;

                DeleteRecord(url, gridName);
            },
            position: "first"

        })
        .navButtonAdd(pager, {/*Translate*/
            caption: "Переклад",
            title: "Переклад",
            buttonicon: " ui-icon-shuffle",
            onClickButton: function () {

                var selr = $(gridName).jqGrid('getGridParam', 'selrow');

                if (GridRowIsNotSelected(gridName, selr)) return;

                if (GridRowTranslated(gridName, selr)) return;

                $('#formgridUniversity' + selr).submit();
            },
            position: "first"
        }).navButtonAdd(pager, {/*Edit*/
            caption: "",
            title: "Редагувати",
            buttonicon: " ui-icon-pencil",
            onClickButton: function () {

                var selr = jQuery(gridName).jqGrid('getGridParam', 'selrow');

                if (GridRowIsNotSelected(gridName, selr)) return;

                window.location.replace(paramFromView.UniversityEditUrl + "/" + selr);
            },
            position: "first"

        }).navButtonAdd(pager, {/*Add*/
            caption: "",
            title: "[Додати]",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                window.location.replace(paramFromView.UniversityCreateUrl);
            },
            position: "first"

        });

});
