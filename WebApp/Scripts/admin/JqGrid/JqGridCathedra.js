﻿$(function () {

    var gridName = "#gridCathedra",
   pager = "#pagerCathedra";


    $(gridName).jqGrid({
        mtype: 'POST',
        datatype: "json",
        caption: '[Кафедри]',
        colModel: [
        { label: 'Id', key: true, hidden: true, name: "Entity.Id", index: 'Entity.Id' },
        { label: 'ParentId', hidden: true, name: "Entity.ParentId", index: 'Entity.ParentId' },
        { label: 'form', hidden: true, formatter: SetEntityForm, formatoptions: { url: paramFromView.CathedraTranslateUrl, entityName: 'Entity', property: 'ParentId' } },
        { label: 'Status', hidden: true, name: "Status" },
        { label: paramFromView.Name, name: "Entity.Name", index: 'Entity.Name', align: 'left', width: 600, formatter: OriginalNameFormat },
        {
            label: paramFromView.Translated,
            sortable: false,
            name: "Translated",

            formatter: GetTranslatedLanguages,
            formatoptions: { property: 'ParentId', entity: true, name: 'CultureFrom', select_style: 'LanguageFrom', containse: true },
        },
        {
            label: paramFromView.NotTranslate,
            sortable: false,
            name: "CultureTo",
            formatter: GetTranslatedLanguages,
            formatoptions: { property: 'ParentId', entity: true, name: 'CultureTo', select_style: 'LanguageTo', containse: false },
        }
        ],
        viewrecords: true,
        pager: $(pager),
        rowNum: 10,
        rowList: [10, 20, 50],
        height: '300',
        sortname: 'Entity.Name',
        sortorder: 'asc',

        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            userdata: "languages",
            repeatitems: false
        },
        loadComplete: function () {
            $('.LanguageFrom').msDropDown();
            $('.LanguageTo').msDropDown();
        },
    });
    jQuery(gridName).jqGrid('navGrid', pager, {
        edit: false, add: false, del: false, search: false, refresh: false,
    })
         .navButtonAdd("#pagerDepartament", {/*Delete button*/
             caption: "",
             title: "[Вид.]",
             // buttonicon: "fa fa-trash-o",
             onClickButton: function () {

                 var selr = jQuery(gridName).jqGrid('getGridParam', 'selrow');
                 if (GridRowIsNotSelected(gridName, selr)) return;

                 var myRowData = $(gridName).jqGrid("getRowData", selr);

                 var url = paramFromView.DepartamentDeleteUrl + "/" + myRowData.ParentID;

                 DeleteRecord(url, gridName);
             },
             position: "first"

         })
        .navButtonAdd(pager, {/*Add button*/
            caption: "",
            title: "Додати",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {
                window.location.replace(paramFromView.CathedraCreateUrl);
            },
            position: "first"
        }).navButtonAdd(pager, {/*Edit button*/
            caption: "",
            title: "Редагувати",
            buttonicon: " ui-icon-pencil",
            onClickButton: function () {

                var selr = jQuery(gridName).jqGrid('getGridParam', 'selrow');

                if (GridRowIsNotSelected(gridName, selr)) return;

                window.location.replace(paramFromView.CathedraEditUrl + "/" + selr);
            },
            position: "first"
        }).navButtonAdd(pager, {/*Translate button*/
            caption: "Переклад",
            title: "Переклад",
            buttonicon: " ui-icon-shuffle",
            onClickButton: function () {

                var selr = $(gridName).jqGrid('getGridParam', 'selrow');

                if (GridRowIsNotSelected(gridName, selr)) return;

                if (GridRowTranslated(gridName, selr)) return;

                var myRowData = $(gridName).jqGrid("getRowData", selr);

                $('#formgridCathedra' + myRowData["Entity.ParentId"]).submit();
            },
            position: "first"
        });

});
