﻿$(function () {

    var gridName = "#gridApp",
        pager = "#pagerApp";

    $(gridName).jqGrid({
        mtype: 'POST',
        url: paramFromView.Url,
        datatype: "json",
        caption: paramFromView.Caption,
        colModel: [
        { label: 'Id', key: true, hidden: true, name: "Entity.Id" },
        { label: 'form', hidden: true, formatter: SetEntityForm, formatoptions: { url: paramFromView.AppTranslateUrl, property: 'Id', entityName: 'Entity' } },
         { label: 'version', name: "Entity.Version", align: 'center' },
        { label: 'Price', name: "Entity.Price", align: 'center' },
        { label: 'Downloads', name: "Entity.Downloads", align: 'center' },
        { label: paramFromView.View, name: "Entity.Views", align: 'center', width: 100 },
        { label: paramFromView.DateTime, name: "Entity.TimeCreated", align: 'center', formatter: 'date', formatoptions: { srcformat: 'Y-m-d H:i:s', newformat: 'd F Y' } },
        { label: paramFromView.Translated, sortable: false, name: "Translated", formatter: GetTranslatedLanguages, formatoptions: { property: 'Id',entity:'Entity', name: 'CultureFrom', select_style: 'LanguageFrom', containse: true }, },
        { label: paramFromView.NotTranslate, sortable: false, name: "CultureTo", formatter: GetTranslatedLanguages, formatoptions: { property: 'Id',entity:'Entity', name: 'CultureTo', select_style: 'LanguageTo', containse: false }, }
        ],
        viewrecords: true,
        pager: $(pager),
        rowNum: 10,
        rowList: [10, 20, 50],
        width: '100%',
        height: '300',
        sortname: 'Entity.Id',
        sortorder: 'asc',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            userdata: "languages",
            repeatitems: false
        },
        loadComplete: function () {
            $('.LanguageFrom').msDropDown();
            $('.LanguageTo').msDropDown();
        },
    });

    jQuery(gridName).jqGrid('navGrid', pager, {
        edit: false, add: false, del: false, search: false, refresh: false,
    })
        .navButtonAdd(pager, {/*Del*/
            caption: "",
            title: "Вид.",

            onClickButton: function () {

                var selr = $(gridName).jqGrid('getGridParam', 'selrow');

                if (GridRowIsNotSelected(gridName, selr)) return;

                var url = paramFromView.AppDeleteUrl + "?id=" + selr;

                DeleteRecord(url, gridName);
            },
            position: "first"

        })
        .navButtonAdd(pager, {/*Translate*/
            caption: "Переклад",
            title: "Переклад",
            buttonicon: " ui-icon-shuffle",
            onClickButton: function () {

                var selr = $(gridName).jqGrid('getGridParam', 'selrow');

                if (GridRowIsNotSelected(gridName, selr)) return;

                if (GridRowTranslated(gridName, selr)) return;


                $('#form' + gridName.substring(1, gridName.length) + selr).submit();
            },
            position: "first"
        }).navButtonAdd(pager, {/*Edit*/
            caption: "",
            title: "Редагувати",
            buttonicon: " ui-icon-pencil",
            onClickButton: function () {

                var selr = jQuery(gridName).jqGrid('getGridParam', 'selrow');

                if (GridRowIsNotSelected(gridName, selr)) return;

                window.location.replace(paramFromView.AppEditUrl + "/" + selr);
            },
            position: "first"

        }).navButtonAdd(pager, {/*Add*/
            caption: "",
            title: "[Додати]",
            buttonicon: "ui-icon-plus",
            onClickButton: function () {

                window.location.replace(paramFromView.AppCreateUrl);
            },
            position: "first"

        }); 
});
