﻿using EasyTest.Entity;
using EasyTest.Service;
using EasyTestData;
using Ninject;
using Ninject.Syntax;
using Ninject.Web.Common;
using Repository.Pattern.DataContext;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace WebApp.Infrastructure
{
	public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public IKernel Kernel
        {
            get { return kernel; }
        }

        public NinjectDependencyResolver()
        {
            kernel = new StandardKernel();
            AddBindings();
        }


        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        public IBindingToSyntax<T> Bind<T>()
        {
            return kernel.Bind<T>();
        }
        private void AddBindings()
        {

            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            Bind<IDataContextAsync>().To<EasyTestContext>().InSingletonScope();
            Bind<IUnitOfWorkAsync>().To<UnitOfWork>().InSingletonScope();




            #region Service
            Bind<INewsService>().To<NewsService>();
            Bind<IRepositoryAsync<News>>().To<RepositoryAsync<News>>();

            Bind<IUniversityService>().To<UniversityService>();
            Bind<IRepositoryAsync<University>>().To<RepositoryAsync<University>>();

            Bind<ICountryService>().To<CountryService>();
            Bind<IRepositoryAsync<Country>>().To<RepositoryAsync<Country>>();

            Bind<ILanguageService>().To<LanguageService>();
            Bind<IRepositoryAsync<Language>>().To<RepositoryAsync<Language>>();

            //Bind<IReleaseService>().To<ReleaseService>();
            //Bind<IRepositoryAsync<App>>().To<RepositoryAsync<App>>();


			Bind<IDepartamentService>().To<DepartamentService>();
			Bind<IRepositoryAsync<Departament>>().To<RepositoryAsync<Departament>>();


			Bind<ICathedraService>().To<CathedraService>();
			Bind<IRepositoryAsync<Cathedra>>().To<RepositoryAsync<Cathedra>>();

            Bind<IPageService>().To<PageService>();
            Bind<IRepositoryAsync<Page>>().To<RepositoryAsync<Page>>();

            Bind<IUserService>().To<UserService>();
            Bind<IRepositoryAsync<User>>().To<RepositoryAsync<User>>();

            Bind<ILogInService>().To<LogInService>();
            Bind<IRepositoryAsync<LogInSingleton>>().To<RepositoryAsync<LogInSingleton>>();


			#endregion





		}
    }
}