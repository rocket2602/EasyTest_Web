﻿using EasyTest.Entity;
using Repository.Pattern.Ef6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Areas.AdminAuth.Models
{
    public class TranslateEntityViewModel<T> where T : Entity
    {
        public T Entity { get; set; }

        public List<Language> SupportedLanguages { get; set; }

        public Int32[] Translated { get; set; }

        public Int32 CultureFrom { get; set; }
    }
}