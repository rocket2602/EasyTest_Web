﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApp.Areas.AdminAuth.Models
{
    public class FileBrowserViewModel
    {
        public IEnumerable<DirectoryInfo> Directories { get; set; }
        public IEnumerable<FileInfo> Files { get; set; }

        public String CurrentDirectory { get; set; }
    }
}