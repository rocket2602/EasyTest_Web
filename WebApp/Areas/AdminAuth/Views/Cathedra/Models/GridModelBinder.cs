﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace WebApp.Areas.AdminAuth.Models
{
    public class GridModelBinder :IModelBinder
    {

     


        public object BindModel(ControllerContext modelBindingExecutionContext, ModelBindingContext bindingContext)
        {
            try
            {
                var request = modelBindingExecutionContext.HttpContext.Request;
                return new GridSettings
                {
                    PageIndex = int.Parse(request["page"] ?? "1"),
                    PageSize = int.Parse(request["rows"] ?? "10"),
                    SortColumn = request["sidx"] ?? "",
                    SortOrder = request["sord"] ?? "asc",
                };
            }
            catch
            {
                return null;
            }
        }

    

}
}