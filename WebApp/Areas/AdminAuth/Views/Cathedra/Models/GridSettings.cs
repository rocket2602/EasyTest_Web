﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.Helpers;

namespace WebApp.Areas.AdminAuth.Models
{
    public class GridSettings
    {
        public Int32 PageIndex { get; set; }
        public Int32 PageSize { get; set; }
        public String SortColumn { get; set; }
        public String SortOrder { get; set; }

        public IQueryable<T> LoadGridData<T>(IQueryable<T> dataSource, out int count)
        {
            var query = dataSource;
            //
            // Sorting and Paging by using the current grid settings.
            //
            query = query.OrderBy<T>(this.SortColumn, this.SortOrder);
            count = query.Count();
            //
            if (this.PageIndex < 1)
                this.PageIndex = 1;
            //
            var data = query.Skip((this.PageIndex - 1) * this.PageSize).Take(this.PageSize);
            return data;
        }
    
    }
}