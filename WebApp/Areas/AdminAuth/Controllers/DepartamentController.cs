﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EasyTest.Entity;
using Repository.Pattern.Infrastructure;
using EasyTest.Entity.ViewModel;
using WebApp.Areas.AdminAuth.Models;
using EasyTest.Service;
using EasyTest.BL.Helpers;

namespace WebApp.Areas.AdminAuth.Controllers
{
    public class DepartamentController : AdminController
    {
        private readonly IDepartamentService _departamentService;

        private readonly ICathedraService _cathedraService;

        public DepartamentController(IDepartamentService departamentService, ICathedraService cathedraService)
        {
            _departamentService = departamentService;

            _cathedraService = cathedraService;

        }

        [HttpPost]
        public JsonResult GetTodoList(GridSettings grid)
        {
            var query = grid.LoadGridData<UniversityIncludeViewModel<Departament>>(_departamentService.GetTodoList(langID), out Count);

            var data = query.ToArray();

            var jsonData = new
            {
                total = (int)Math.Ceiling((double)Count / grid.PageSize),
                page = grid.PageIndex,
                records = Count,
                rows = data.Select(x => new
                {
                    Entity = new
                    {
                        Id = x.Entity.Id,
                        Name = x.Entity.Name,
                        ParentId = x.Entity.ParentId,
                        LanguageId = x.Entity.LanguageId
                    },

                    Translated = x.Translated,
                    Status = x.Status
                }),
                languages = GetSupportedLanguages().Select(x =>
                    new
                    {
                        Id = x.ID,
                        Icon = x.Icon,
                        NativeName = x.NativeName
                    })
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #region DepartamentHierarchy

        [HttpPost]
        public ActionResult GetDepartaments(GridSettings grid, Int32 Id)
        {
            /*Id == UniversityId*/
            var departaments = grid.LoadGridData<UniversityIncludeViewModel<Departament>>(_departamentService.GetDepartamentsByUniversityId(Id, langID), out Count);

            if (Request.IsAjaxRequest())
            {
                var data = departaments.ToArray();

                var jsonData = new
                {
                    total = (int)Math.Ceiling((double)Count / grid.PageSize),
                    page = grid.PageIndex,
                    records = Count,
                    rows = data.Select(x => new
                    {
                        Entity = new
                        {
                            Id = x.Entity.Id,
                            Name = x.Entity.Name,
                            ParentId = x.Entity.ParentId,
                        },

                        Status = x.Status,
                        Translated = x.Translated
                    }),
                };
                return Json(jsonData);

            }
            return Content("");
        }

        [HttpGet]
        public ActionResult DepartamentCathedra(Int32? id, Int32? UniversityID)
        {
            /*Id==ParentId*/

            if (id == null || UniversityID == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var departament = _departamentService.Query(x => x.ParentId == id).Include(i => i.DepartamentsCathedras).Select().FirstOrDefault();

            if (departament == null) return HttpNotFound();

            var selected = departament.DepartamentsCathedras.Select(x => x.CathedraId).ToList();

            CathedraDropDownList(selected);

            ViewData["UniversityID"] = UniversityID;

            return View(departament);
        }

        private void CathedraDropDownList(List<Int32> selected = null)
        {
            var cathedra = _cathedraService.GetAll(langID)
                                           .OrderByDescending(x => x.Name);

            ViewBag.Cathedra = new MultiSelectList(cathedra, "ParentId", "Name", selected);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DepartamentCathedra(Int32 Id, Int32[] selectedCathedra, Int32 UniversityID)
        {
            /*Id==ParentId*/

            var departament = (await _departamentService.Query(x => x.ParentId == Id )// &&  x.DepartamentsCathedras.Any(c => c.UniversityId == UniversityID && c.DepartamentId == Id) )
                                                  .Include(i => i.DepartamentsCathedras)
                                                  .SelectAsync()).FirstOrDefault();


            UpdateDepartamentCathedra(departament, selectedCathedra, UniversityID);


            _departamentService.Update(departament);

            try
            {
                await _unitOfWorkAsync.SaveChangesAsync();

                return Json(true);
            }
            catch
            {
                return Json("Error. Don`t add departaments");

            }

        }

        private void UpdateDepartamentCathedra(Departament model, Int32[] selectedCathedra, Int32 UniversityID)
        {
            if (selectedCathedra == null)
            {
                model.DepartamentsCathedras = new List<DepartamentsCathedra>();
                return;
            }

            var selectedCathedraHS = new HashSet<Int32>(selectedCathedra);

            var departamentCathedra = new HashSet<Int32>(model.DepartamentsCathedras.Where(x=>x.UniversityId==UniversityID).Select(c => c.CathedraId));

            foreach (var item in _cathedraService.GetAll(langID).ToList())
            {
                var obj = new DepartamentsCathedra { UniversityId = UniversityID, DepartamentId = model.ParentId, CathedraId = item.ParentId };

                if (selectedCathedraHS.Contains(item.ParentId))
                {
                    if (!departamentCathedra.Contains(item.ParentId))
                    {
                        obj.ObjectState = ObjectState.Added;       
                        model.DepartamentsCathedras.Add(obj);
                    }
                }
                else
                {
                    if (departamentCathedra.Contains(item.ParentId))
                    {
                        obj.ObjectState = ObjectState.Deleted;
                        model.DepartamentsCathedras.Remove(obj);
                    }
                }
            }
        }


        [HttpPost]
        public async Task<ActionResult> DeleteCathedraInHierarchy(Int32? UniversityId,Int32? DepartamentParentId,Int32? CathedraParentId)
        {
            if (UniversityId == null || DepartamentParentId == null || CathedraParentId==null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var departament = _departamentService.Query(x => x.DepartamentsCathedras.Any(d => d.UniversityId == UniversityId && d.CathedraId == CathedraParentId && d.DepartamentId == DepartamentParentId))
                                                         .Include(i=>i.DepartamentsCathedras)
                                                         .Select()
                                                         .FirstOrDefault();

            if (departament == null)
            {
                Response.StatusCode = 500;

                return Content("Record not found.");
            }

          var departamentCathedra =   departament.DepartamentsCathedras.First();

            departamentCathedra.ObjectState = ObjectState.Deleted;


            departament.DepartamentsCathedras.Remove(departamentCathedra);

            _departamentService.Update(departament);

            await SaveChangesAsync();

            return Json(true);



        }


        #endregion

        #region CRUD

        public ActionResult Create()
        {
            var model = new Departament();

            ViewData["success"] = TempData["message"] as String;


            return View(InsertOrUpdateView, model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name")] Departament model, Boolean stay)
        {
            if (ModelState.IsValid)
            {
                model.LanguageId = langID;

                model.ObjectState = ObjectState.Added;

                _departamentService.Insert(model);

                await SaveChangesAsync();

                if (stay)
                {
                    TempData["message"] = "[Запис успішно додано]";

                    return RedirectToAction("Create");
                }

                return RedirectToAction("Index", "University", new { Grid = "Departament" });
            }

            return View(InsertOrUpdateView, model);
        }


        public async Task<ActionResult> Edit(Int32? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var departament = await _departamentService.FindAsync(id);

            if (departament == null) return HttpNotFound();

            return View(InsertOrUpdateView, departament);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Departament model)
        {
            if (ModelState.IsValid)
            {
                var departament = await _departamentService.FindAsync(model.Id);

                TryUpdateModel(departament, new String[] { "Name" });

                departament.TimeUpdated = DateTime.Now.ToUnixTime();

                departament.ObjectState = ObjectState.Modified;

                _departamentService.Update(departament);

                await SaveChangesAsync();

                return RedirectToAction("Index", "University", new { Grid = "Departament" });
            }

            return View(InsertOrUpdateView, model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(Int32? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!Request.IsAjaxRequest()) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);


            var departament = (await _departamentService.Query(x => x.ParentId == id)
                                                        .SelectAsync())
                                                        .ToList();

            if (departament == null)
            {
                Response.StatusCode = 500;

                return Json("[Record not found.]");
            }

            departament.ForEach(x =>
            {
                x.ObjectState = ObjectState.Deleted;

                _departamentService.Delete(x);
            });

            await SaveChangesAsync();

            return Json(true);
        }

        #endregion

        #region Translate

        public ActionResult GetOriginal(Int32 ParentId, Int32 CultureFrom)
        {
            var model = _departamentService.GetOriginal(ParentId, CultureFrom);


            if (Request.IsAjaxRequest())
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            var vm = new TranslateEntityViewModel<Departament>()
            {
                Entity = model,
                Translated = _departamentService.GetTranslated(langID),
                SupportedLanguages = GetSupportedLanguages()
            };


            return View("_Original", vm);
        }

        [HttpPost]
        public ActionResult Translate(Int32? id, Int32? CultureFrom, Int32? CultureTo)
        {
            /* id=ParentId */
            if (id == null || CultureFrom == null || CultureTo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Відсутні параметри");
            }

            var translated = _departamentService.GetTranslated(id.Value);

            if (translated == null) return new HttpNotFoundResult();

            bool isTranslated = translated.Contains(CultureFrom.Value);

            bool isNotTranslated = translated.Contains(CultureTo.Value);

            if (isNotTranslated && isNotTranslated) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Невірні параметри");

            var vm = new TranslateEntityViewModel<Departament>()
            {
                Entity = new Departament()
                {
                    ParentId = id.Value,
                    LanguageId = CultureTo.Value
                },
                Translated = _departamentService.GetTranslated(id.Value),
                SupportedLanguages = GetSupportedLanguages(),
                CultureFrom = CultureFrom.Value
            };

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveTranslate(TranslateEntityViewModel<Departament> vm)
        {
            if (ModelState.IsValid)
            {
                vm.Entity.ObjectState = ObjectState.Added;

                _departamentService.Insert(vm.Entity);

                await SaveChangesAsync();

                return RedirectToAction("Index", "University", new { Grid = "Departament" });
            }

            vm.SupportedLanguages = GetSupportedLanguages();

            vm.Translated = _departamentService.GetTranslated(vm.Entity.ParentId);

            return View(vm);
        }

        #endregion

    }
}
