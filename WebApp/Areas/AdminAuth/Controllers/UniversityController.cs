﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using EasyTest.Service;
using Repository.Pattern.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.Areas.AdminAuth.Models;
using EasyTest.BL.Helpers;

namespace WebApp.Areas.AdminAuth.Controllers
{
    public class UniversityController : AdminController
    {
        private readonly IUniversityService _universityService;

        private readonly IDepartamentService _departamentService;

        public UniversityController(IUniversityService universityService, IDepartamentService departamentService)
        {
            _universityService = universityService;
            _departamentService = departamentService;
        }

        public ActionResult Index()
        {
            ViewData["success"] = TempData["message"] as String;

            return View();
        }

        [HttpPost]
        public JsonResult GetTodoList(GridSettings grid)
        {
            var query = grid.LoadGridData<UniversityViewModel>(_universityService.GetTodoList(langID), out Count);

            var data = query.ToArray();



            var jsonData = new
            {
                total = (int)Math.Ceiling((double)Count / grid.PageSize),
                page = grid.PageIndex,
                records = Count,

                rows = data.Select(x =>
                   new
                   {
                       x.Id,
                       CountryId = Resource.Extend.GetString(x.Country.Name),
                       LMSId = x.LMS.Name,
                       Domain = x.Domain,
                       Status = x.Status,
                       UniversitiesLang = new UniversitiesLang
                       {
                           Name = x.UniversitiesLang.Name,
                           City = x.UniversitiesLang.City,
                       },
                       DateTime = x.TimeCreated.ToDateTime(),
                       x.Translated,
                   }),
                languages = GetSupportedLanguages().Select(x =>
                    new
                    {
                        Id = x.ID,
                        Icon = x.Icon,
                        NativeName = x.NativeName
                    })
            };
            return Json(jsonData);
        }

        #region UniversityHierarchy

        [HttpPost]
        public JsonResult GetUniversityList(GridSettings grid)
        {
            var query = grid.LoadGridData<UniversityViewModel>(_universityService.GetTodoList(langID), out Count);

            var data = query.ToArray();

            var jsonData = new
            {
                total = (int)Math.Ceiling((double)Count / grid.PageSize),
                page = grid.PageIndex,
                records = Count,
                rows = data.Select(x =>
                    new
                    {
                        x.Id,
                        CountryId = Resource.Extend.GetString(x.Country.Name),
                        LMSId = x.LMS.Name,
                        x.Domain,
                        x.Status,
                        DateTime = x.TimeCreated.ToDateTime(),

                        UniversitiesLang = new UniversitiesLang
                        {
                            Name = x.UniversitiesLang.Name,
                            City = x.UniversitiesLang.City,
                        },
                    })
            };

            return Json(jsonData);
        }

        [HttpGet]
        public ActionResult UniversityDepartaments(Int32? Id)
        {        
            if (Id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var university = _universityService.Query(x => x.Id == Id)
                                                      .Include(i => i.Departaments)
                                                      .Select()
                                                      .FirstOrDefault();

            if (university == null) return HttpNotFound();

            var selected = university.Departaments.Select(x => x.ParentId).ToList();

            DepartamentsDropDownList(selected);

            return View(university);
        }

        private void DepartamentsDropDownList(List<Int32> selected = null)
        {
            var departaments = _departamentService.GetAll(langID)
                                                  .OrderByDescending(x => x.Name);

            ViewBag.Departaments = new MultiSelectList(departaments, "ParentId", "Name", selected);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UniversityDepartaments(Int32 Id, Int32[] selectedDepartaments)
        {

            var data = _universityService.Query(x => x.Id == Id)
                                                .Include(i => i.Departaments)
                                                .Select()
                                                .FirstOrDefault();

            if (data == null) return HttpNotFound();

            UpdateUniversityDepartaments(data, selectedDepartaments);

            data.ObjectState = ObjectState.Modified;

            _universityService.Update(data);

            try
            {
                _unitOfWorkAsync.SaveChanges();

                return Json(true);
            }
            catch (Exception e)
            {
                return Json("Error. Don`t add departaments " + e.Message);

            }
        }

        private void UpdateUniversityDepartaments(University model, Int32[] selectedDepartaments)
        {
            if (selectedDepartaments == null)
            {
                model.Departaments = new List<Departament>();

                return;
            }

            var selectedDepartamentsHS = new HashSet<Int32>(selectedDepartaments);

            var universityDepartaments = new HashSet<Int32>(model.Departaments.Select(c => c.ParentId));

            foreach (var item in _departamentService.GetAll(langID).ToList())
            {
                var obj = item;
             //   obj.Id = obj.ParentId;

                if (selectedDepartamentsHS.Contains(item.ParentId))
                {
                    if (!universityDepartaments.Contains(item.ParentId))
                    {
                        model.Departaments.Add(obj);
                    }
                }
                else
                {
                    if (universityDepartaments.Contains(item.ParentId))
                    {
                        model.Departaments.Remove(obj);
                    }
                }
            }
        }

        [HttpPost]
        public async Task<ActionResult> DeleteDepartamentInHierarchy(Int32? UniversityId, Int32? DepartamentParentId)
        {
            if (UniversityId == null || DepartamentParentId == null || !Request.IsAjaxRequest()) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var university = (await _universityService.Query(x => x.Departaments.Any(d=>d.Id== DepartamentParentId))
                                                      .Include(i => i.Departaments)
                                                      .SelectAsync())
                                                      .FirstOrDefault();

            if (university == null)
            {
                Response.StatusCode = 500;

                return Content("Record not found.");
            }

            var universityDepartament = university.Departaments.First();

            university.Departaments.Remove(universityDepartament);

            university.ObjectState = ObjectState.Modified;

            _universityService.Update(university);

            await SaveChangesAsync();

            return Json(true);
        }

        #endregion

        #region CRUD

        public ActionResult Create()
        {

            var model = new UniversityViewModel()
            {
                Countries = _countryService.GetAll(langID),
            };

            ViewData["success"] = TempData["message"] as String;


            LMSDropDownList();

            return View(InsertOrUpdateView, model);
        }

        private void LMSDropDownList(Int32 selected = 0)
        {
            var lms = _unitOfWorkAsync.RepositoryAsync<LMS>()
                                      .Queryable()
                                      .OrderBy(x => x.Name);

            ViewBag.LMS = new SelectList(lms, "Id", "Name", selected);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UniversityViewModel vm, University university)
        {
            if (ModelState.IsValid)
            {
                vm.UniversitiesLang.LanguageId = langID;

                university.TimeCreated = DateTime.Now.ToUnixTime();

                university.UniversitiesLangs.Add(vm.UniversitiesLang);

                university.ObjectState = ObjectState.Added;

                vm.UniversitiesLang.ObjectState = ObjectState.Added;

                _universityService.InsertOrUpdateGraph(university);

                await SaveChangesAsync();

                TempData["message"] = "[Запис успішно додано]";

                return RedirectToAction("Index", new { Grid = "University" });
            }

            LMSDropDownList(vm.LMSID);

            return View(InsertOrUpdateView, vm);
        }

        public ActionResult Edit(Int32? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var vm = _universityService.FindById(id.Value, langID);

            if (vm == null) return HttpNotFound();

            vm.Countries = _countryService.GetAll(langID);


            LMSDropDownList(vm.LMSID);

            return View(InsertOrUpdateView, vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UniversityViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var model = _universityService.FindById(vm.Id, langID);

                TryUpdateModel(model, new String[] { "CountryId", "LMSID", "Domain" });

                TryUpdateModel(model.UniversitiesLang, "UniversitiesLang", new String[] { "Name", "City" });

                var university = new University()
                {

                    Id = model.Id,
                    CountryId = model.CountryId,
                    LMSID = model.LMSID,
                    Domain = model.Domain,
                    Status = model.Status,
                    TimeCreated = model.TimeCreated,
                    ObjectState = ObjectState.Modified

                };

                model.UniversitiesLang.ObjectState = ObjectState.Modified;

                university.UniversitiesLangs.Add(model.UniversitiesLang);

                _universityService.InsertOrUpdateGraph(university);

                await SaveChangesAsync();

                return RedirectToAction("Index", new { Grid = "University" });
            }

            vm.Countries = _countryService.GetAll(langID);

            LMSDropDownList(vm.LMSID);

            return View(InsertOrUpdateView, vm);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(Int32? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!Request.IsAjaxRequest())
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var university = await _universityService.FindAsync(id);

            if (university == null)
            {
                Response.StatusCode = 500;

                return Json("[Record not found.]");
            }

            university.ObjectState = ObjectState.Deleted;

            _universityService.Delete(university);

            try
            {
                await SaveChangesAsync();

                return Json(true);
            }
            catch (Exception e)
            {
                return Json("error" + e.Message);
            }


        }

        #endregion

        #region Translate


        public async Task<ActionResult> GetOriginal(Int32 id, Int32 CultureFrom)
        {
            var university = _universityService.GetUniversitiesLang(id, CultureFrom);

            if (Request.IsAjaxRequest()) return Json(await university, JsonRequestBehavior.AllowGet);

            var vm = new TranslateEntityViewModel<UniversitiesLang>()
            {
                Entity = await university,
                Translated = _universityService.GetTranslatedLanguages(id),
                SupportedLanguages = GetSupportedLanguages()
            };

            return View("_Original", vm);
        }

        [HttpPost]
        public ActionResult Translate(Int32? id, Int32? CultureFrom, Int32? CultureTo)
        {

            if (id == null || CultureFrom == null || CultureTo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Відсутні параметри");
            }

            var Translated = _universityService.GetTranslatedLanguages(id.Value);

            if (Translated == null)
            {
                return HttpNotFound();
            }

            bool IsTranslated = Translated.Contains(CultureFrom.Value);
            bool IsNotTranslated = !Translated.Contains(CultureTo.Value);

            if (IsTranslated && !IsNotTranslated)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "p from та To невірні");

            }

            var model = new UniversitiesLang()
            {
                UniversityId = id.Value,
                LanguageId = CultureTo.Value,
            };

            var vm = new TranslateEntityViewModel<UniversitiesLang>()
            {
                Entity = model,
                CultureFrom = CultureFrom.Value,
                SupportedLanguages = GetSupportedLanguages(),
                Translated = Translated
            };


            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveTranslate(TranslateEntityViewModel<UniversitiesLang> vm)
        {
            if (ModelState.IsValid)
            {
                vm.Entity.ObjectState = ObjectState.Added;

                _unitOfWorkAsync.RepositoryAsync<UniversitiesLang>().Insert(vm.Entity);

                await _unitOfWorkAsync.SaveChangesAsync();

                return RedirectToAction("Index", new { Grid = "University" });
            }

            vm.SupportedLanguages = GetSupportedLanguages();

            vm.Translated = _universityService.GetTranslatedLanguages(vm.Entity.UniversityId);

            return View(vm);
        }

        #endregion
    }
}
