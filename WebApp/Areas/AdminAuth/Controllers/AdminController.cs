﻿using EasyTest.Entity;
using EasyTest.Service;
using Ninject;
using Repository.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using Microsoft.AspNet.Identity;
using System.Net.NetworkInformation;

namespace WebApp.Areas.AdminAuth.Controllers
{
  //  [Authorize]
	//[Authorize(Roles="admin")]
    public abstract  class AdminController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if(User.Identity.IsAuthenticated)
            //{

            //     String sid = (Session["sid"] != null) ? Session["sid"].ToString() : "";

            //    var userId = Convert.ToInt32(User.Identity.GetUserId());


            //    if (_logInService.IsYourLoginStillTrue(userId, sid))
            //    {
            //        if (_logInService.IsUserLoggedOnElseWhere(userId, GetMAC()))
            //        {
            //            _logInService.KillOtherSession(userId, GetMAC());
            //        }
            //    }
            //    else
            //    {
            //        HttpContext.GetOwinContext().Authentication.SignOut();
            //        Response.Redirect(Url.Action("UserIsLoginAnyWhere", "Account", new { area = "" }));
            //    }

            //    GetUserName();
            //}

            
            base.OnActionExecuting(filterContext);
        }

        private String GetMAC()
        {
            string macAddresses = "";

            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    macAddresses += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }
            return macAddresses;
        }

        public Int32 Count;

		public string InsertOrUpdateView { get { return "InsertOrUpdate"; } }

        

        [OutputCache(Duration = 60)]
        private void GetUserName()
        {

            Int32 userID = Convert.ToInt32(User.Identity.GetUserId());

            var user = _userService.Query(x => x.Id == userID)
                                    .Select()
                                    .SingleOrDefault();

           // ViewBag.Username = user.FirstName + " "+ user.LastName;

        }

	

	}
}