﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EasyTest.Entity;
using Repository.Pattern.Infrastructure;
using EasyTest.Service;
using EasyTest.Entity.ViewModel;
using EasyTest.BL.Helpers;
using Microsoft.AspNet.Identity;
using WebApp.Areas.AdminAuth.Models;

namespace WebApp.Areas.AdminAuth.Controllers
{
    public class ReleaseController : AdminController
    {

        private readonly IAppService _appService;

        public ReleaseController(IAppService appService)
        {
            _appService = appService;
        }


        public  ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetTodoList(GridSettings grid)//Index
        {
            var query = grid.LoadGridData<AppViewModel>(_appService.GetTodoList(langID), out Count);

            var data = query.ToArray();

            var jsonData = new
            {
                total = (int)Math.Ceiling((double)Count / grid.PageSize),
                page = grid.PageIndex,
                records = Count,
                rows = data.Select(x => new
                {
                    Entity = new {
                        x.Entity.Id,
                        x.Entity.Version,
                        x.Entity.Price,
                        x.Entity.Views,
                      
                        x.Entity.Downloads,
                        TimeCreated = x.Entity.TimeCreated.ToDateTime(),
                    },
                    x.Translated,
                }),
                languages = GetSupportedLanguages().Select(x =>
                    new
                    {
                        Id = x.ID,
                        Icon = x.Icon,
                        NativeName = x.NativeName
                    })
            };

            return Json(jsonData);
        }


        public ActionResult DownloadFile()
        {
            return View();
        }






        //// GET: AdminAuth/Releasy/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    App app = await db.Apps.FindAsync(id);
        //    if (app == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(app);
        //}


        public ActionResult Create()
        {
            var model = new AppViewModel() { DateTime = DateTime.Now };


            return View(InsertOrUpdateView, model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Exclude = "Id,Views,Downloads")] AppViewModel vm, String[] change)
        {
            var changes = String.Join("|", change);

            vm.AppChange.Changes = changes;

            if (ModelState.IsValid)
            {
                vm.Entity.TimeCreated = vm.DateTime.ToUnixTime();

                vm.Entity.UserCreated = 13;// User.Identity.GetUserId<Int32>();

                vm.Entity.ObjectState = ObjectState.Added;

                var appChanges = new AppChanges()
                {
                    Changes = changes,
                    LanguageId = langID,
                    ObjectState = ObjectState.Added
                };

                vm.Entity.AppChanges.Add(appChanges);

                _appService.Insert(vm.Entity);

                await SaveChangesAsync();

                return RedirectToAction("Index");
            }

            return View(InsertOrUpdateView, vm);
        }

        // GET: AdminAuth/Releasy/Edit/5
        public  ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var app =  _appService.GetById(id.Value,langID);

            if (app == null)
            {
                return HttpNotFound();
            }

            return View(InsertOrUpdateView, app);
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AppViewModel vm, String[] change)
        {
            var changes = String.Join("|", change);

            vm.AppChange.Changes = changes;

            if (ModelState.IsValid)
            {
                var model = _appService.GetById(vm.Entity.Id,langID);

                if (model == null) return HttpNotFound();

                TryUpdateModel(model.Entity,"Entity",new String[] { "Portable", "Full", "Version", "Price", "IsSupported", "IsCurrent", "VideoLinks", "Screenshots" });
          
                model.Entity.UserUpdated = 13;//change in release todo

                model.Entity.TimeUpdated = DateTime.Now.ToUnixTime();

                model.AppChange.ObjectState = ObjectState.Modified;

                model.Entity.AppChanges.Add(model.AppChange);

                model.Entity.ObjectState = ObjectState.Modified;

                _appService.Update(model.Entity);

                await SaveChangesAsync();

                return RedirectToAction("Index");
            }
                  
            return View(InsertOrUpdateView, vm);
        }

        //// GET: AdminAuth/Releasy/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    App app = await db.Apps.FindAsync(id);
        //    if (app == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(app);
        //}

        //// POST: AdminAuth/Releasy/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    App app = await db.Apps.FindAsync(id);
        //    db.Apps.Remove(app);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}


        public PartialViewResult _VideoLinks()
        {
            return PartialView();
        }


        public PartialViewResult _UploadRelease(bool portable = false)
        {
            ViewBag.Type = portable;
            ViewBag.Field = portable ? "Entity_Portable" : "Entity_Full";

            return PartialView();
        }


        public PartialViewResult _UploadScreenshots()
        {
            ViewBag.Thumb = "/App/Screenshots/Thumb/";

            return PartialView();

        }


        [HttpPost]
        public ActionResult Translate(Int32? id, Int32? CultureFrom, Int32? CultureTo)
        {

            if (id == null || CultureFrom == null || CultureTo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Відсутні параметри");
            }

            var Translated = _appService.GetTranslated(id.Value);

            if (Translated == null)
            {
                return HttpNotFound();
            }

            bool IsTranslated = Translated.Contains(CultureFrom.Value);
            bool IsNotTranslated = !Translated.Contains(CultureTo.Value);

            if (IsTranslated && !IsNotTranslated)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "p from та To невірні");

            }

       

            var vm = new TranslateEntityViewModel<AppChanges>()
            {
                Entity = new AppChanges() {
                    AppId=id.Value,
                    LanguageId = CultureTo.Value
                },
                CultureFrom = CultureFrom.Value,
                SupportedLanguages = GetSupportedLanguages(),
                Translated = Translated
            };


            return View(vm);
        }

        public ActionResult GetOriginal(Int32 id, Int32 CultureFrom)
        {
            var changes = _appService.GetAppChange(id, CultureFrom);

            if (Request.IsAjaxRequest())
            {
                return Json(changes, JsonRequestBehavior.AllowGet);
            }

            var vm = new TranslateEntityViewModel<AppChanges>()
            {
                Entity = changes,
                Translated = _appService.GetTranslated(id),
                SupportedLanguages = GetSupportedLanguages()
            };

            return View("_Original", vm);
        }

        [HttpPost]
        public async Task<ActionResult> SaveTranslate(TranslateEntityViewModel<AppChanges> vm)
        {
            if (ModelState.IsValid)
            {
                vm.Entity.Changes = vm.Entity.Changes.Replace("\n","|");
                
                vm.Entity.ObjectState = ObjectState.Added;


                _unitOfWorkAsync.RepositoryAsync<AppChanges>().Insert(vm.Entity);

                await SaveChangesAsync();

                return RedirectToAction("Index");
            }

            vm.SupportedLanguages = GetSupportedLanguages();
            vm.Translated = _appService.GetTranslated(vm.Entity.AppId);


            return View("Translate", vm);
        }



    }
}
