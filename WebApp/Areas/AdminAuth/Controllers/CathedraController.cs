﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EasyTest.Entity;
using Repository.Pattern.Infrastructure;
using EasyTest.Entity.ViewModel;
using WebApp.Areas.AdminAuth.Models;
using EasyTest.Service;
using EasyTest.BL.Helpers;

namespace WebApp.Areas.AdminAuth.Controllers
{
    public class CathedraController : AdminController
    {
        private readonly ICathedraService _cathedraService;


        public CathedraController(ICathedraService cathedraService)
        {
            _cathedraService = cathedraService;
        }

        [HttpPost]
        public JsonResult GetTodoList(GridSettings grid)
        {

            var query = grid.LoadGridData<UniversityIncludeViewModel<Cathedra>>(_cathedraService.GetTodoList(langID), out Count);

            var data = query.ToArray();

            var jsonData = new
            {
                total = (int)Math.Ceiling((double)Count / grid.PageSize),
                page = grid.PageIndex,
                records = Count,
                rows = data.Select(x => new
                {
                    Entity = new
                    {
                        Id = x.Entity.Id,
                        Name = x.Entity.Name,
                        ParentId = x.Entity.ParentId,
                        LanguageId = x.Entity.LanguageId
                    },
                    Translated = x.Translated,
                    Status = x.Status
                }),
                languages = GetSupportedLanguages().Select(x =>
                    new
                    {
                        Id = x.ID,
                        Icon = x.Icon,
                        NativeName = x.NativeName
                    })
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCathedra(GridSettings grid, Int32? id, Int32? UniversityID)
        {

            if (id == null || UniversityID == null) return new HttpNotFoundResult();

            var cathedra = grid.LoadGridData<UniversityIncludeViewModel<Cathedra>>(_cathedraService.GetCathedra(UniversityID.Value, id.Value, langID), out Count);

            if (Request.IsAjaxRequest())
            {
                var data = cathedra.ToArray();

                var jsonData = new
                {
                    total = (int)Math.Ceiling((double)Count / grid.PageSize),
                    page = grid.PageIndex,
                    records = Count,
                    rows = data.Select(x => new
                    {
                        Entity = new
                        {
                            x.Entity.Id,
                            x.Entity.Name,
                            x.Entity.ParentId,

                        },
                        x.Status,
                        x.Translated,
                    }),
                };
                return Json(jsonData);
            }

            return Json("Something wrong");
        }
    
        #region CRUD

        public ActionResult Create()
        {
            var model = new Cathedra();

            ViewData["success"] = TempData["message"] as String;

            return View(InsertOrUpdateView, model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name")] Cathedra model, Boolean stay)
        {
            if (ModelState.IsValid)
            {
                model.LanguageId = langID;

                model.ObjectState = ObjectState.Added;

                _cathedraService.Insert(model);

                await SaveChangesAsync();

                if (stay)
                {
                    TempData["message"] = "[Запис успішно додано]";

                    return RedirectToAction("Create");
                }
                return RedirectToAction("Index", "University", new { Grid = "Cathedra" });
            }
            return View(InsertOrUpdateView, model);
        }

        public async Task<ActionResult> Edit(Int32? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var model = (await _cathedraService.Query(x=>x.Id==id).SelectAsync()).FirstOrDefault();

            if (model == null) return HttpNotFound();

            return View(InsertOrUpdateView, model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Cathedra model)
        {
            if (ModelState.IsValid)
            {
                var cathedra = await _cathedraService.FindAsync(model.Id);

                TryUpdateModel(cathedra, new String[] { "Name" });

                cathedra.TimeUpdated = DateTime.Now.ToUnixTime();

                cathedra.ObjectState = ObjectState.Modified;

                _cathedraService.Update(cathedra);

                await SaveChangesAsync();

                return RedirectToAction("Index", "University", new { Grid = "Cathedra" });
            }

            return View(InsertOrUpdateView, model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(Int32 id)
        {
            if (id == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);


            var cathedra = (await _cathedraService.Query(x => x.ParentId == id)
                                                  .SelectAsync())
                                                  .ToList();
            if (cathedra == null)
            {
                Response.StatusCode = 500;

                return Json("[Record not found.]");
            }

            cathedra.ForEach(x =>
            {
                x.ObjectState = ObjectState.Deleted;

                _cathedraService.Delete(x);
            });

            await SaveChangesAsync();

            return Json(true);
        }

        #endregion

        #region Translate

        public ActionResult GetOriginal(Int32 ParentId, Int32 CultureFrom)
        {
            var model = _cathedraService.GetOriginal(ParentId, CultureFrom);

            if (Request.IsAjaxRequest())
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            var vm = new TranslateEntityViewModel<Cathedra>()
            {
                Entity = model,
                Translated = _cathedraService.GetTranslated(langID),
                SupportedLanguages = GetSupportedLanguages()
            };

            return View("_Original", vm);
        }

        [HttpPost]
        public ActionResult Translate(Int32? id, Int32? CultureFrom, Int32? CultureTo)
        {
            /* id=ParentId */
            if (id == null || CultureFrom == null || CultureTo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Відсутні параметри");
            }

            var translated = _cathedraService.GetTranslated(id.Value);

            if (translated == null) return new HttpNotFoundResult();

            bool isTranslated = translated.Contains(CultureFrom.Value);

            bool isNotTranslated = translated.Contains(CultureTo.Value);

            if (isNotTranslated && isNotTranslated) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Невірні параметри");

            var vm = new TranslateEntityViewModel<Cathedra>()
            {
                Entity = new Cathedra()
                {
                    ParentId = id.Value,
                    LanguageId = CultureTo.Value
                },
                Translated = _cathedraService.GetTranslated(id.Value),
                SupportedLanguages = GetSupportedLanguages(),
                CultureFrom = CultureFrom.Value
            };

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveTranslate(TranslateEntityViewModel<Cathedra> vm)
        {
            if (ModelState.IsValid)
            {
                vm.Entity.ObjectState = ObjectState.Added;

                _cathedraService.Insert(vm.Entity);

                await SaveChangesAsync();

                return RedirectToAction("Index", "University", new { Grid = "Cathedra" });
            }

            vm.SupportedLanguages = GetSupportedLanguages();

            vm.Translated = _cathedraService.GetTranslated(vm.Entity.ParentId);

            return View(vm);
        }

        #endregion
    }
}
