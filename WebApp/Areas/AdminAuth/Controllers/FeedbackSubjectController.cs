﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EasyTest.Entity;
using Repository.Pattern.Infrastructure;

namespace WebApp.Areas.AdminAuth.Controllers
{
    public class FeedbackCategoryController : AdminController
    {
      
        public async Task<ActionResult> Index()
        {
			
			var model = _unitOfWorkAsync.RepositoryAsync<FeedbackCategory>().Queryable();
            return View(await model.ToListAsync());
        }

       
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var FeedbackCategory = await _unitOfWorkAsync.RepositoryAsync<FeedbackCategory>().FindAsync(id);
            if (FeedbackCategory == null)
            {
                return HttpNotFound();
            }
            return View(FeedbackCategory);
        }

       
        public ActionResult Create()
        {
            return View();
        }

 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name")] FeedbackCategory model)
        {
            if (ModelState.IsValid)
            {
				_unitOfWorkAsync.RepositoryAsync<FeedbackCategory>().Insert(model);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(model);
        }

       
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var FeedbackCategory = await _unitOfWorkAsync.RepositoryAsync<FeedbackCategory>().FindAsync(id);
            if (FeedbackCategory == null)
            {
                return HttpNotFound();
            }
            return View(FeedbackCategory);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name")] FeedbackCategory model)
        {
            if (ModelState.IsValid)
            {
				model.ObjectState = ObjectState.Modified;
                _unitOfWorkAsync.RepositoryAsync<FeedbackCategory>().Update(model);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(model);
        }

       
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var FeedbackCategory = await _unitOfWorkAsync.RepositoryAsync<FeedbackCategory>().FindAsync(id);
            if (FeedbackCategory == null)
            {
                return HttpNotFound();
            }
            return View(FeedbackCategory);
        }

       
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var FeedbackCategory = await _unitOfWorkAsync.RepositoryAsync<FeedbackCategory>().FindAsync(id);
			_unitOfWorkAsync.RepositoryAsync<FeedbackCategory>().Delete(FeedbackCategory);
            await _unitOfWorkAsync.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}
