﻿using EasyTest.Entity;
using EasyTest.Entity.ViewModel;
using Repository.Pattern.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Areas.AdminAuth.Controllers
{
    public class PageController : AdminController
    {
        
        public ActionResult Index(Int32 id = 1)
        {
            var page =  _pageService.FindPageByID(id,langID);


            if (page == null)
            {
                return new HttpNotFoundResult();
            }

            GetPageDropDown(page.ID);

            ViewData["message"] = TempData["success"] as String;

            if (Request.IsAjaxRequest())
            {
                return Json(page, JsonRequestBehavior.AllowGet);
            }

            return View(page);
        }

        private void GetPageDropDown(Int32 selected)
        {
            var pages = _pageService.Queryable().Select(x => new { x.ID, x.Title });
            ViewBag.Pages = new SelectList(pages, "ID", "Title", selected);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(PageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var pageLang = (await _unitOfWorkAsync.RepositoryAsync<PageLang>()
                                                      .Query(x => x.PageID == model.PageID && x.LanguageID == langID)
                                                      .SelectAsync())
                                                      .FirstOrDefault();
                if (pageLang == null)
                {
                    return new HttpNotFoundResult();
                }

                TryUpdateModel(pageLang,new String[]{"Name","Description","Keywords","Text"});
                pageLang.ObjectState = ObjectState.Modified;
                _unitOfWorkAsync.RepositoryAsync<PageLang>().Update(pageLang);

                await _unitOfWorkAsync.SaveChangesAsync();

                TempData["success"] = "[Зміни успішно внесено]";
                return RedirectToAction("Index", new { id = model.PageID });

            }
            else
            {
                GetPageDropDown(model.PageID);
                return View("Index", model);
            }

        }

        
    }
}