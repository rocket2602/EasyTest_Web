﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WebApp.Areas.AdminAuth.Models;

namespace WebApp.Areas.AdminAuth.Controllers
{
    public class UploadController : Controller
    {
        #region Fields

        private const Int32 NewsStoredWidth = 640;  // ToDo - Change the size of the stored avatar image
        private const Int32 NewsStoredHeight = 240; // ToDo - Change the size of the stored avatar image
        private const Int32 NewsScreenWidth = 700;  // ToDo - Change the value of the width of the image on the screen


        private const string TempFolder = "/Temp";

        private const String _baseFolder = @"\Content\Images";
        private const String _newsImage = @"\Content\Images\News\";
        private const String _releaseImages = @"\Content\Images\Release\";
        private const String _patchToApp = @"\App\";
        private const String _ScreenshotsApp = @"\App\Screenshots\";

        private HttpPostedFileBase file;
        private String fileName, filePath, errorMessage;
        private bool success;


        private readonly string[] _imageFileExtensions = { ".jpg", ".png", ".gif", ".jpeg" };
        #endregion



        public ActionResult _UploadNewsImage()
        {
            return PartialView();
        }



        [HttpGet]
        public ActionResult _UploadImage()
        {
            return PartialView("UploadImage");
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadRelease(HttpPostedFileBase files, bool type)
        {
            if (files == null) return Json(new { success = false, errorMessage = Resource.GlobalRes.NoFileUploaded });

            if (files.ContentLength <= 0) return Json(new { success = false, errorMessage = Resource.GlobalRes.FileCantBeZero });

            if (!files.ContentType.Equals("application/octet-stream")) return Json(new { success = false, errorMessage = Resource.GlobalRes.FileWrongFormat });

            this.file = files;

            String appType = type ? "Portable" : "Install";

            fileName = RandomFileName("app_" + appType);

            filePath = Path.Combine(_patchToApp + appType, fileName);

            SaveFile();

            return Json(new { success = success, errorMessage = errorMessage, fileName = fileName.Replace("/", "\\"), filePath = filePath });
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadMultiImage(IEnumerable<HttpPostedFileBase> files)
        {
            var allFileName = new HashSet<String>();

            foreach (var item in files)
            {
                if (item == null) return Json(new { success = false, errorMessage = Resource.GlobalRes.NoFileUploaded });

                if (item == null || !IsImage(item)) return Json(new { success = false, errorMessage = Resource.GlobalRes.FileWrongFormat });

                if (item.ContentLength <= 0) return Json(new { success = false, errorMessage = Resource.GlobalRes.FileCantBeZero });

                this.file = item;

                fileName = RandomFileName("Screenshots");

                filePath = Path.Combine(_ScreenshotsApp, "Original", fileName);

                SaveFile();

                if (success)
                {
                   var thumb =  CreateThumbnail(150, 150, Path.Combine(_ScreenshotsApp, "Thumb", fileName));

                   var gallery =  CreateThumbnail(900, 600, Path.Combine(_ScreenshotsApp, "Gallery", fileName));

                    if (thumb && gallery)
                    {
                        allFileName.Add(fileName);
                    }
                }
            }

            fileName = String.Join("|",allFileName);


            return Json(new { success = success, message = errorMessage, fileName = fileName }, "text/html");
        }



        public struct JsonCropData
        {
            public Int32 x, y, height, width, rotate;

            public JsonCropData(JObject jsonObject)
            {

                this.x = Convert.ToInt32(jsonObject["x"]);
                this.y = Convert.ToInt32(jsonObject["y"]);
                this.width = Convert.ToInt32(jsonObject["width"]);
                this.height = Convert.ToInt32(jsonObject["height"]);
                this.rotate = Convert.ToInt32(jsonObject["rotate"]);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadImage(HttpPostedFileBase files, String avatar_data)
        {
            // String folder = "News";
            if (files == null) return Json(new { success = false, errorMessage = Resource.GlobalRes.NoFileUploaded });
            this.file = files;
            if (file == null || !IsImage(file)) return Json(new { success = false, errorMessage = Resource.GlobalRes.FileWrongFormat });
            if (file.ContentLength <= 0) return Json(new { success = false, errorMessage = Resource.GlobalRes.FileCantBeZero });

            JObject jsonObject = (JObject)JsonConvert.DeserializeObject(avatar_data);

            var crop = new JsonCropData(jsonObject);


            fileName = RandomFileName("news");
            filePath = Path.Combine(_newsImage, fileName);

            if (Directory.Exists(filePath) == false)
            {
                Directory.CreateDirectory(filePath);
            }

            CropAndSaveImage(crop);


            return Json(new { success = true, fileName = fileName.Replace("/", "\\"), filePath = filePath });
        }

        private void CropAndSaveImage(JsonCropData crop)
        {
            var image = Image.FromStream(file.InputStream);

            var newImage = new Bitmap(crop.width, crop.height);
            Graphics thumbGraph = Graphics.FromImage(newImage);

            thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbGraph.RotateTransform(crop.rotate);
            thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

            thumbGraph.DrawImage(image, new Rectangle(0, 0, crop.width, crop.height),
                                        new Rectangle(crop.x, crop.y, crop.width, crop.height), GraphicsUnit.Pixel);
            image.Dispose();


            newImage.Save(Server.MapPath(filePath), newImage.RawFormat);
        }


        private void SaveFile()
        {
            try
            {

                file.SaveAs(Server.MapPath(filePath));
                success = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                success = false;
            }
        }

        private bool IsImage(HttpPostedFileBase file)
        {
            if (file == null) return false;

            return file.ContentType.Contains("image") ||
                _imageFileExtensions.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }


        private string RandomFileName(String prefix)
        {

            String fileName = String.Format("{0}_{1}_{2}{3}", prefix,
                                                              DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                                                              Guid.NewGuid().ToString(),
                                                              Path.GetExtension(file.FileName));
            return fileName;
        }






        private String GetTempSavedFilePath(HttpPostedFileBase file)
        {
            var serverPath = HttpContext.Server.MapPath(TempFolder);
            if (Directory.Exists(serverPath) == false)
            {
                Directory.CreateDirectory(serverPath);
            }

            // Generate unique file name
            var fileName = Path.GetFileName(file.FileName);
            fileName = SaveTemporaryNewsFileImage(file, serverPath, fileName);

            // Clean up old files after every save
            CleanUpTempFolder(1);
            return Path.Combine(TempFolder, fileName);
        }

        private void CleanUpTempFolder(Int32 hoursOld)
        {
            try
            {
                var currentUtcNow = DateTime.UtcNow;
                var serverPath = HttpContext.Server.MapPath("/Temp");
                if (!Directory.Exists(serverPath)) return;
                var fileEntries = Directory.GetFiles(serverPath);
                foreach (var fileEntry in fileEntries)
                {
                    var fileCreationTime = System.IO.File.GetCreationTimeUtc(fileEntry);
                    var res = currentUtcNow - fileCreationTime;
                    if (res.TotalHours > hoursOld)
                    {
                        System.IO.File.Delete(fileEntry);
                    }
                }
            }
            catch
            {
                // Deliberately empty.
            }
        }

        private static string SaveTemporaryNewsFileImage(HttpPostedFileBase file, String serverPath, String fileName)
        {
            var img = new WebImage(file.InputStream);
            var ratio = img.Height / (double)img.Width;
            img.Resize(NewsScreenWidth, (int)(NewsScreenWidth * ratio));

            var fullFileName = Path.Combine(serverPath, fileName);
            if (System.IO.File.Exists(fullFileName))
            {
                System.IO.File.Delete(fullFileName);
            }

            img.Save(fullFileName);
            return Path.GetFileName(img.FileName);
        }

        private bool CreateThumbnail(Int32 maxWidth, Int32 maxHeight, String pachTo)
        {
            try
            {

                var image = System.Drawing.Image.FromFile(Server.MapPath(filePath));
                var ratioX = (double)maxWidth / image.Width;
                var ratioY = (double)maxHeight / image.Height;
                var ratio = Math.Min(ratioX, ratioY);
                var newWidth = (int)(image.Width * ratio);
                var newHeight = (int)(image.Height * ratio);
                var newImage = new Bitmap(newWidth, newHeight);
                Graphics thumbGraph = Graphics.FromImage(newImage);

                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

                thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
                image.Dispose();


                newImage.Save(Server.MapPath(pachTo), newImage.RawFormat);
                return true;
            }
            catch (Exception e)
            {
                errorMessage += e.Message;
                return false;
            }

        }
























        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult UploadRelease(HttpPostedFileBase files)
        //{
        //    this.file = files;
        //    if (FileIsValid(file, ".zip"))
        //    {
        //        fileName = RandomFileName("App");
        //        filePath = Path.Combine(_patchToApp, fileName);
        //        SaveFile();
        //    }

        //    return Json(new { Uploaded = IsUpload, message = message, fileName = fileName, filePath = filePath }, "text/html");
        //}



        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult UploadMultiImage(IEnumerable<HttpPostedFileBase> files)
        //{

        //    var AllFileName = new List<String>();
        //    var AllFilePath = new List<String>();

        //    foreach (var item in files)
        //    {

        //        this.file = item;
        //        if (FileIsValid(file, ".png", ".jpeg", ".jpg"))
        //        {


        //                fileName = RandomFileName("Release");

        //                filePath = Path.Combine(_releaseImages, "Original", fileName);

        //              SaveFile();


        //                if (IsUpload)
        //                {

        //                    String filePathThumb = Path.Combine(_releaseImages, "Thumb", fileName);

        //                    CreateThumbnail(150, 150, filePathThumb);
        //                    // Gallery
        //                    CreateThumbnail(900, 600,  Path.Combine(_releaseImages, "Gallery", fileName));

        //                    AllFileName.Add(fileName);
        //                    AllFilePath.Add(filePathThumb);
        //                }

        //        }
        //    }
        //     fileName = String.Join(",", AllFileName);
        //    filePath = String.Join(",", AllFilePath);

        //    return Json(new { Uploaded = IsUpload, message = message, fileName = fileName, filePath = filePath }, "text/html");
        //}



        public ActionResult FilebrowserBrowse(String directory = "/Content/Images/News")
        {
            String[] allowExtention = { ".png", ".jpg", ".gif" };
            String pach = Server.MapPath("~" + directory);
            var model = new FileBrowserViewModel();
            model.CurrentDirectory = directory + "/";
            model.Directories = GetSubDirectories(pach);
            var dir = new DirectoryInfo(pach);
            model.Files = dir.EnumerateFiles().Where(x => allowExtention.Contains(x.Extension));
            return View(model);
        }

        private DirectoryInfo[] GetSubDirectories(String directoryPach)
        {

            if (Directory.Exists(directoryPach))
            {
                var directory = new DirectoryInfo(directoryPach).GetDirectories();
                return directory;
            }

            return null;


        }

        //[HttpPost]
        //public void CkeUpload(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        //{

        //    this.file = upload;
        //    if (FileIsValid(upload, ".png", ".jpeg", ".jpg"))
        //    {

        //         fileName = RandomFileName("Image");
        //        filePath = Path.Combine(_newsImage ,fileName);
        //        SaveFile();
        //        message = "Файл успішно завантажено";
        //    }

        //    Response.Write(String.Format("<script>window.parent.CKEDITOR.tools.callFunction({0}, \"{1}\", \"{2}\");</script>", CKEditorFuncNum, "~"+filePath, message));
        //    Response.End();
        //}


        [HttpPost]
        public JsonResult Delete(String path)
        {
            var absolutePath = Server.MapPath(path);
            FileInfo Image = new FileInfo(absolutePath);
            if (Image.Exists) Image.Delete();
            return Json(new { success = true });
        }

        [HttpPost]
        public JsonResult DeleteScrinshot(String file)
        {
            String[] files = new String[3];

            files[0] = Server.MapPath(Path.Combine(_ScreenshotsApp, "Original", file));
            files[1] = Server.MapPath(Path.Combine(_ScreenshotsApp, "Thumb", file));
            files[2] = Server.MapPath(Path.Combine(_ScreenshotsApp, "Gallery", file));

            foreach (var item in files)
            {
                FileInfo Image = new FileInfo(item);
                if (Image.Exists) Image.Delete();
            }

            return Json(new { success = true });
        }
    }
}