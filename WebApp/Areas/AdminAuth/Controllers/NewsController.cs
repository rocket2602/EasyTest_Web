﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using EasyTest.Entity;
using Repository.Pattern.Infrastructure;
using EasyTest.Entity.ViewModel;
using WebApp.Areas.AdminAuth.Models;
using EasyTest.BL.Helpers;
using EasyTest.Service;
using Microsoft.AspNet.Identity;
using WebApp.Helpers;

namespace WebApp.Areas.AdminAuth.Controllers
{
    public class NewsController : AdminController
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }


        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetTodoList(GridSettings grid)//Index
        {
            var query = grid.LoadGridData<NewsViewModel>(_newsService.GetTodoList(langID), out Count);
  
            var data = query.ToArray();

            var jsonData = new
            {
                total = (int)Math.Ceiling((double)Count / grid.PageSize),
                page = grid.PageIndex,
                records = Count,
                rows = data.Select(x => new
                {
                    Id = x.News.Id,
                    Image = x.News.Image,
                    Name = x.NewsLang.Name,
                    UserId = x.News.UserId,
                    CategoryName = x.NewsCategory.Name,
                    Views = x.NewsLang.View,
                    TimeCreated = x.News.TimeCreated.ToDateTime(),
                    Translated = x.Translated
                }),
                languages = GetSupportedLanguages().Select(x =>
                    new
                    {
                        Id = x.ID,
                        Icon = x.Icon,
                        NativeName = x.NativeName
                    })
            };

            return Json(jsonData);
        }

        #region CRUD

        public ActionResult Create()
        {
            var vm = new NewsViewModel()
            {
                DateTime = DateTime.Now,
            };

            vm.News.Image = "notImage.png";

            NewsCategoriesDropDownList();

            return View(InsertOrUpdateView, vm);
        }

        private void NewsCategoriesDropDownList(Int32 selected = 0)
        {
            var categories = _newsService.GetNewsCategories();

            ViewBag.NewsCategories = new SelectList(categories, "Id", "Name", selected);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(NewsViewModel vm) 
        {
            if (ModelState.IsValid)
            {
                vm.News.UserId = User.Identity.GetUserId<Int32>();

                vm.News.TimeCreated = vm.DateTime.ToUnixTime();

                vm.NewsLang.LanguageId = langID;

                vm.News.ObjectState = ObjectState.Added;

                vm.NewsLang.ObjectState = ObjectState.Added;

                vm.News.NewsLangs.Add(vm.NewsLang);

                _newsService.InsertOrUpdateGraph(vm.News);

                await SaveChangesAsync();

                return RedirectToAction("Index");
            }

            NewsCategoriesDropDownList(vm.News.CategoryID);

            return View(InsertOrUpdateView, vm);
        }

        public ActionResult Edit(Int32? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var vm = _newsService.FindNewsByID(id.Value, langID, false);

            if (vm == null) return HttpNotFound();

            vm.DateTime = vm.News.TimeCreated.ToDateTime();

            NewsCategoriesDropDownList(vm.News.CategoryID);

            return View(InsertOrUpdateView, vm);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(NewsViewModel vm)
        {
            if (ModelState.IsValid)
            {              
                var model = _newsService.FindNewsByID(vm.News.Id,langID,false);

                TryUpdateModel(model.News, "News",new String[] {"Image","CategoryID"});

                TryUpdateModel(model.NewsLang, "NewsLang", new String[] { "Name","ShortText","Text","Keywords" });

                if (IsUpdateTimeAllow(model.News.TimeCreated, model.News.UserId))
                {
                    model.News.TimeCreated = vm.DateTime.ToUnixTime();
                }

                model.NewsLang.UpdatedBy = User.Identity.GetUserId<Int32>();

                model.NewsLang.TimeUpdated = DateTime.Now.ToUnixTime();

                model.News.NewsLangs.Add(model.NewsLang);

                model.News.ObjectState = ObjectState.Modified;

                _newsService.InsertOrUpdateGraph(model.News);

                await SaveChangesAsync();

                return RedirectToAction("Index");
            }

            NewsCategoriesDropDownList(vm.News.CategoryID);

            return View(InsertOrUpdateView, vm);
        }

        private bool IsUpdateTimeAllow(long timeCreated, int author)
        {
            var isPublished = timeCreated <= DateTime.Now.ToUnixTime();

            return author == User.Identity.GetUserId<Int32>() && !isPublished;       
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var news = await _newsService.FindAsync(id);

            if (news == null)
            {
                return HttpNotFound();
            }

            _newsService.Delete(news);

            await SaveChangesAsync();

            return Json(true);
        }


        #endregion

  
        [HttpPost]
        public ActionResult Translate(Int32? id, Int32? CultureFrom, Int32? CultureTo)
        {
            if (id == null || CultureFrom == null || CultureTo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Відсутні параметри");
            }

            var translated = _newsService.GetTranslated(id.Value);

            if(translated==null) return new HttpNotFoundResult();

            bool isTranslated = translated.Contains(CultureFrom.Value);

            bool isNotTranslated = translated.Contains(CultureTo.Value);

            if(isNotTranslated && isNotTranslated) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Невірні параметри");

            var vm = new TranslateEntityViewModel<NewsLang>()
            {
                Entity = new NewsLang()
                {
                    NewsId = id.Value,
                    LanguageId = CultureTo.Value
                },
                Translated = translated,
                SupportedLanguages = GetSupportedLanguages(),
                CultureFrom = CultureFrom.Value
        };
                
            return View(vm);
        }

        public ActionResult GetOriginalNews(Int32 id, Int32 CultureFrom)
        {
            var newLang = _newsService.GetNewsLang(id, CultureFrom);

            if (Request.IsAjaxRequest())
            {
                return Json(newLang, JsonRequestBehavior.AllowGet);
            }

            var vm = new TranslateEntityViewModel<NewsLang>()
            {
                Entity = newLang,
                Translated = _newsService.GetTranslated(id),
                SupportedLanguages = GetSupportedLanguages()
            };
      
            return View("_Original", vm);
        }

        [HttpPost]
        public async Task<ActionResult> SaveTranslate(TranslateEntityViewModel<NewsLang> vm)
        {
            if (ModelState.IsValid)
            {
                vm.Entity.TimeUpdated = DateTime.Now.ToUnixTime();

                vm.Entity.UpdatedBy = User.Identity.GetUserId<Int32>();

                vm.Entity.ObjectState = ObjectState.Added;
           
                _unitOfWorkAsync.RepositoryAsync<NewsLang>().Insert(vm.Entity);

                await _unitOfWorkAsync.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            vm.SupportedLanguages = GetSupportedLanguages();
            vm.Translated = _newsService.GetTranslated(vm.Entity.NewsId);


            return View("Translate",vm);
        }

     
    }
}
