﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EasyTest.Entity;
using Repository.Pattern.Infrastructure;

namespace WebApp.Areas.AdminAuth.Controllers
{
	public class NewsCategoryController : AdminController
	{

		public async Task<ActionResult> Index()
		{
			var model = _unitOfWorkAsync.RepositoryAsync<NewsCategory>().Queryable();
			return View(await model.ToListAsync());
		}


		public async Task<ActionResult> Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var newsCategory = await _unitOfWorkAsync.RepositoryAsync<NewsCategory>().FindAsync(id);
			if (newsCategory == null)
			{
				return HttpNotFound();
			}
			return View(newsCategory);
		}


		public ActionResult Create()
		{
			return View();
		}


		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create([Bind(Include = "Name")] NewsCategory model)
		{
			if (ModelState.IsValid)
			{
				model.ObjectState = ObjectState.Added;
				_unitOfWorkAsync.RepositoryAsync<NewsCategory>().Insert(model);
				

				await _unitOfWorkAsync.SaveChangesAsync();
				return RedirectToAction("Index");
			}

			return View(model);
		}


		public async Task<ActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var newsCategory = await _unitOfWorkAsync.RepositoryAsync<NewsCategory>().FindAsync(id);
			if (newsCategory == null)
			{
				return HttpNotFound();
			}
			return View(newsCategory);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit([Bind(Include = "ID,Name")] NewsCategory model)
		{
			if (ModelState.IsValid)
			{
				model.ObjectState = ObjectState.Modified;
				_unitOfWorkAsync.RepositoryAsync<NewsCategory>().Update(model);
			
				await _unitOfWorkAsync.SaveChangesAsync();
				return RedirectToAction("Index");
			}
			return View(model);
		}

		public async Task<ActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var newsCategory = await _unitOfWorkAsync.RepositoryAsync<NewsCategory>().FindAsync(id);
			if (newsCategory == null)
			{
				return HttpNotFound();
			}
			return View(newsCategory);
		}

		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> DeleteConfirmed(int id)
		{
			var newsCategory = await _unitOfWorkAsync.RepositoryAsync<NewsCategory>().FindAsync(id);
			_unitOfWorkAsync.RepositoryAsync<NewsCategory>().Delete(newsCategory);
			await _unitOfWorkAsync.SaveChangesAsync();
			return RedirectToAction("Index");
		}


	}
}
