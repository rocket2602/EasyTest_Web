﻿using System.Web.Mvc;
using WebApp.Helpers;

namespace WebApp.Areas.AdminAuth
{
    public class AdminAuthAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AdminAuth";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AdminAuth_default",
                "AdminAuth/{culture}/{controller}/{action}/{id}",
                new {culture= CultureHelper.GetDefaultCulture(), controller="Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}