﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;

namespace WebApp.Areas.WebInterface.Controllers
{
    public class HomeController : BaseController
    {
        // GET: WebInterface/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}