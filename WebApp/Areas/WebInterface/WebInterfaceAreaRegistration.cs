﻿using System.Web.Mvc;
using WebApp.Helpers;

namespace WebApp.Areas.WebInterface
{
    public class WebInterfaceAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "WebInterface";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "WebInterface_default",
                "WebInterface/{culture}/{controller}/{action}/{id}",
                new { culture = CultureHelper.GetDefaultCulture(),  controller="Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}